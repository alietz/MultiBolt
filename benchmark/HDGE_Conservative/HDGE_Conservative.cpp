// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 


// This source file, when compiled, creates several analytic species and tests the program
// using the same kind of validation tables as seen in Stephens (2018)
// Compares against Reid Ramp and Lucas Salee models versus a table of known values from MCC

// these amainly function as validators for the governing equations

#include "multibolt"



int main() {

	


	lib::libSpeaker.printmode_normal_statements();
	mb::mbSpeaker.printmode_normal_statements();

		
	/* Set up parameters for solver --------------------------------------------------------*/

	mb::BoltzmannParameters p = mb::BoltzmannParameters();

	p.model = mb::ModelCode::HDGE_01;
	p.N_terms = 10;

	p.Nu = 1000;

	p.iter_max = 200;	// Force break if you haven't converged within this many iterations to avoid wasting time
	p.iter_min = 4;		// Force continue if below this number of iterations
	p.weight_f0 = 1.0;	// Iteration weight. 

	
	p.USE_ENERGY_REMAP = true;		// Look for a new grid if necessary
	p.USE_EV_MAX_GUESS = true;

	p.remap_grid_trial_max = 15;				// try a new grid no more than this many times
	p.remap_target_order_span = 10.0;			// how many orders would you prefer your EEDF to span (in eV^-3/2)

	p.initial_eV_max = 5;

	p.EN_Td = 1;
	p.p_Torr = 760;
	p.T_K = 0;

	p.sharing = 0.5; // energy sharing between primary and secondary electrons in ionization

	/* Perform Boltzmann solver calculation  */

	// instantiate
	mb::BoltzmannSolver run;
	mb::BoltzmannOutput out;

	

	mb::normal_statement("Using following parameters as starting-point for validation:");
	p.print();

	std::vector<double> EN_Tds = { 1, 12, 24 };
	std::vector<double> eV_maxes = { 5, 5, 5 };
	
	// vectors of ouputs:
	std::vector<mb::BoltzmannOutput> RR_HDGE_sols(EN_Tds.size(), mb::BoltzmannOutput());


	#pragma omp parallel
	#pragma omp for
	for (int i = 0; i < EN_Tds.size(); ++i) {

		lib::Library this_rr_lib = lib::get_ReidRamp_Library(); //make a copy
		this_rr_lib.assign_scattering_by_type(lib::isotropic_scattering(), lib::CollisionCode::elastic);
		this_rr_lib.assign_scattering_by_type(lib::isotropic_scattering(), lib::CollisionCode::excitation);

		auto this_p = p; // make a copy

		this_p.EN_Td = EN_Tds.at(i);
		this_p.initial_eV_max = eV_maxes.at(i); // necessary to make full use
		mb::normal_statement("Beginning validation solution: ReidRamp, HD+GE, E/N = " + std::to_string(this_p.EN_Td) + " Td");
		run = mb::BoltzmannSolver(this_p, this_rr_lib);
		RR_HDGE_sols.at(i) = run.get_output();
	}

	


	// Some fake output data structures which will match the MCC comparisons

	// a little painstaking, but it will work

	// See: Raspopovic Z M, Sakadzic S, Bzenic S A and Petrovic Z Lj 1999 IEEE Trans. Plasma Sci. 27 1241
	std::vector<mb::BoltzmannOutput> Raspopovic_HDGE_RR_vec(EN_Tds.size(), mb::BoltzmannOutput());

	// for 1 Td
	Raspopovic_HDGE_RR_vec[0].avg_en = 0.1017;
	Raspopovic_HDGE_RR_vec[0].W_FLUX = 1.273e+4;
	Raspopovic_HDGE_RR_vec[0].DTN_FLUX = 0.966e+24;
	Raspopovic_HDGE_RR_vec[0].DLN_FLUX = 0.7575e+24;

	// for 12 Td
	Raspopovic_HDGE_RR_vec[1].avg_en = 0.2703;
	Raspopovic_HDGE_RR_vec[1].W_FLUX = 6.834e+4;
	Raspopovic_HDGE_RR_vec[1].DTN_FLUX = 1.140e+24;
	Raspopovic_HDGE_RR_vec[1].DLN_FLUX = 0.569e+24;

	// for 24 Td
	Raspopovic_HDGE_RR_vec[2].avg_en = 0.4113;
	Raspopovic_HDGE_RR_vec[2].W_FLUX = 8.804e+4;
	Raspopovic_HDGE_RR_vec[2].DTN_FLUX = 1.131e+24;
	Raspopovic_HDGE_RR_vec[2].DLN_FLUX = 0.4546e+24;

	/* ----------------------------------------------- */

	// See: White R D, Brennan M J, Ness K F 1997 J. Phys. D: Appl. Phys. 30 810
	std::vector<mb::BoltzmannOutput> White_HDGE_RR_vec(EN_Tds.size());

	// for 1 Td
	White_HDGE_RR_vec[0].avg_en = 0.1015;
	White_HDGE_RR_vec[0].W_FLUX = 1.271e+4;
	White_HDGE_RR_vec[0].DTN_FLUX = 0.974e+24;
	White_HDGE_RR_vec[0].DLN_FLUX = 0.757e+24;

	// for 12 Td
	White_HDGE_RR_vec[1].avg_en = 0.2693;
	White_HDGE_RR_vec[1].W_FLUX = 6.833e+4;
	White_HDGE_RR_vec[1].DTN_FLUX = 1.136e+24;
	White_HDGE_RR_vec[1].DLN_FLUX = 0.5816e+24;

	// for 24 Td
	White_HDGE_RR_vec[2].avg_en = 0.4085;
	White_HDGE_RR_vec[2].W_FLUX = 8.878e+4;
	White_HDGE_RR_vec[2].DTN_FLUX = 1.140e+24;
	White_HDGE_RR_vec[2].DLN_FLUX = 0.4684e+24;


	/* ----------------------------------------------- */


	std::vector<std::string> args_strings = { "E/N = 1 Td", "E/N = 12 Td", "E/N = 24 Td" };

	/*   ------    */
	

	for (int i = 0; i < EN_Tds.size(); ++i) {

		



		printf("\n%s", args_strings.at(i).c_str());

		double elemmb = RR_HDGE_sols[i].avg_en;
		double elemref1 = White_HDGE_RR_vec[i].avg_en;
		double elemref2 = Raspopovic_HDGE_RR_vec[i].avg_en;

		double err1 = (elemmb - elemref1) / abs(elemmb);
		double err2 = (elemmb - elemref2) / abs(elemmb);
		printf("\n\tavg_en :\t\tMB: %1.4e\t\tMC: %1.4e\t\tMC: %1.4e\t\t%1.4e%%\t\t%1.4e%%", elemmb, elemref1, elemref2, err1 * 100, err2 * 100);




		elemmb = RR_HDGE_sols[i].W_FLUX;
		elemref1 = White_HDGE_RR_vec[i].W_FLUX;
		elemref2 = Raspopovic_HDGE_RR_vec[i].W_FLUX;

		err1 = (elemmb - elemref1) / abs(elemmb);
		err2 = (elemmb - elemref2) / abs(elemmb);
		printf("\n\tW_FLUX :\t\tMB: %1.4e\t\tMC: %1.4e\t\tMC: %1.4e\t\t%1.4e%%\t\t%1.4e%%", elemmb, elemref1, elemref2, err1 * 100, err2 * 100);



		elemmb = RR_HDGE_sols[i].DTN_FLUX;
		elemref1 = White_HDGE_RR_vec[i].DTN_FLUX;
		elemref2 = Raspopovic_HDGE_RR_vec[i].DTN_FLUX;

		err1 = (elemmb - elemref1) / abs(elemmb);
		err2 = (elemmb - elemref2) / abs(elemmb);
		printf("\n\tDTN_FLUX :\t\tMB: %1.4e\t\tMC: %1.4e\t\tMC: %1.4e\t\t%1.4e%%\t\t%1.4e%%", elemmb, elemref1, elemref2, err1 * 100, err2 * 100);


		elemmb = RR_HDGE_sols[i].DLN_FLUX;
		elemref1 = White_HDGE_RR_vec[i].DLN_FLUX;
		elemref2 = Raspopovic_HDGE_RR_vec[i].DLN_FLUX;

		err1 = (elemmb - elemref1) / abs(elemmb);
		err2 = (elemmb - elemref2) / abs(elemmb);
		printf("\n\tDLN_FLUX :\t\tMB: %1.4e\t\tMC: %1.4e\t\tMC: %1.4e\t\t%1.4e%%\t\t%1.4e%%", elemmb, elemref1, elemref2, err1 * 100, err2 * 100);


		std::cout << std::endl;
	}



	return 0;
}







