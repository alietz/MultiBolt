// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 







// 09/19/2022
// Calculates some Reid Ramp style benchmarks for various analytic scattering treatments

#include "multibolt"


// Not-energy-dependent models
lib::Scattering ModelB_scattering() {

	std::function<double(const double, const double, const double)> foo_Ibar =
		[](const double eV, const double eVprime, const double chi) {


		return  cos(chi) * cos(chi) * cos(chi) * cos(chi) / (0.4 * 2.0 * mb::PI);
	};


	lib::Scattering X = lib::Scattering(foo_Ibar);
	X.name = "Haddad et al 1981, Model B)";

	std::function<double(const double, const double)> foo_ratio = // ratio of cross sections just needs incident electron energy
		[](const double eV, const double eVprime) {

		return 1.0;
	};
	X.user_ratio_elasticTotal_to_elasticMT = foo_ratio;
	X.ratio_USE_SLOW_EVAL = false;
	X.integral_USE_SLOW_EVAL = true;

	return X;
}



// Not-energy-dependent models
lib::Scattering ModelC_scattering() {

	std::function<double(const double, const double, const double)> foo_Ibar =
		[](const double eV, const double eVprime, const double chi) {

		return  std::exp(-1.5 * (1.0 + cos(chi))) / (0.633474422609354 * 2.0 * mb::PI);
	};


	lib::Scattering X = lib::Scattering(foo_Ibar);
	X.name = "Haddad et al 1981, Model C)";


	std::function<double(const double, const double)> foo_ratio = // ratio of cross sections just needs incident electron energy
		[](const double eV, const double eVprime) {

		return arma::datum::nan; // 1.0 / 1.8;
	};
	X.user_ratio_elasticTotal_to_elasticMT = foo_ratio;
	X.ratio_USE_SLOW_EVAL = true;
	X.integral_USE_SLOW_EVAL = true;

	return X;
}

// Not-energy-dependent models
lib::Scattering ModelD_scattering() {

	std::function<double(const double, const double, const double)> foo_Ibar =
		[](const double eV, const double eVprime, const double chi) {

		if (chi <= 0.134 * mb::PI || chi >= 0.75 * mb::PI) {
			return 1.0 / (0.379173877839021 * 2.0 * mb::PI);
		}
		else {
			return 0.0;
		}
	};


	lib::Scattering X = lib::Scattering(foo_Ibar);
	X.name = "Haddad et al 1981, Model D)";


	std::function<double(const double, const double)> foo_ratio = // ratio of cross sections just needs incident electron energy
		[](const double eV, const double eVprime) {

		return arma::datum::nan; // 1.0 / 1.8;
	};
	X.user_ratio_elasticTotal_to_elasticMT = foo_ratio;
	X.ratio_USE_SLOW_EVAL = true;
	X.integral_USE_SLOW_EVAL = true;

	return X;
}




int main() {


	lib::libSpeaker.printmode_normal_statements();
	mb::mbSpeaker.printmode_normal_statements();

	
	/* Set up parameters for solver --------------------------------------------------------*/

	mb::BoltzmannParameters p = mb::BoltzmannParameters();

	p.model = mb::ModelCode::HDGE_01;
	p.N_terms = 10; 

	p.Nu = 1000;	


	p.initial_eV_max = 5;
	p.USE_ENERGY_REMAP = true;		// Look for a new grid if necessary
	p.remap_grid_trial_max = 25;				// try a new grid no more than this many times
	p.remap_target_order_span = 15.0;			// how many orders would you prefer your EEDF to span (in eV^-3/2)


	std::vector<double> vec_EN_Td = { 1, 12, 24 };

	p.EN_Td = 1;
	p.p_Torr = 760;
	p.T_K = 300;

	p.sharing = 0.5; // energy sharing between primary and secondary electrons in ionization

	std::vector<lib::Scattering> vec_ela_scattering = 
		{ lib::isotropic_scattering() , lib::isotropic_scattering() , lib::isotropic_scattering() , lib::screened_coulomb_scattering() };
	std::vector<lib::Scattering> vec_inel_scattering =
	{ lib::isotropic_scattering() , lib::forward_scattering() , lib::screened_coulomb_scattering() , lib::screened_coulomb_scattering() };


	/* Perform Boltzmann solver calculation  */

	// instantiate
	mb::BoltzmannSolver run;
	mb::BoltzmannOutput out;

	

	mb::normal_statement("Using following parameters as starting-point for validation:");
	p.print();

	std::vector<std::string> args_EN_strings = { "E/N = 1 Td", "E/N = 12 Td", "E/N = 24 Td"};
	std::vector<std::string> args_scatter_strings = { "iso-iso", "iso-fwd", "iso-sc", "sc-sc" };

	// vectors of ouputs
	std::vector<std::vector<mb::BoltzmannOutput>> RR_scatter_sols(vec_EN_Td.size(), std::vector<mb::BoltzmannOutput>(vec_ela_scattering.size(), mb::BoltzmannOutput()));




	lib::libSpeaker.printmode_normal_statements();
	mb::mbSpeaker.printmode_normal_statements();

	#pragma omp parallel
	#pragma omp for
	for (int i = 0; i < vec_EN_Td.size(); ++i) {
	
		/* Develop library of analytic species -------------------------------------------------------- */
		lib::Library this_RR = lib::get_ReidRamp_Library();
	
		mb::BoltzmannParameters this_p = p;
	
		this_p.EN_Td = vec_EN_Td.at(i);
	
		for (int j = 0; j < vec_ela_scattering.size(); ++j) {
	
	
			this_RR.assign_scattering_by_type(vec_ela_scattering.at(j), lib::CollisionCode::elastic);
			this_RR.assign_scattering_by_type(vec_inel_scattering.at(j), lib::CollisionCode::excitation);
	
			mb::normal_statement("Beginning validation solution: ReidRamp, HD, E/N = " + std::to_string(this_p.EN_Td) + " Td " + args_scatter_strings.at(j));
			run = mb::BoltzmannSolver(this_p, this_RR);
			RR_scatter_sols.at(i).at(j) = run.get_output();
	
		}
	
	}
	
	
	
	
	for (int i = 0; i < vec_EN_Td.size(); ++i) {
	
		printf("\n%s", args_EN_strings.at(i).c_str());
	
		for (int j = 0; j < vec_ela_scattering.size(); ++j) {
	
			printf("\n\t%s", args_scatter_strings.at(j).c_str());
	
			
			printf("\n\t\tavg_en :\t\tMB: %1.4e", RR_scatter_sols.at(i).at(j).avg_en);
			printf("\n\t\tW_FLUX :\t\tMB: %1.4e", RR_scatter_sols.at(i).at(j).W_FLUX);
			printf("\n\t\tDTN_FLUX :\t\tMB: %1.4e", RR_scatter_sols.at(i).at(j).DTN_FLUX);
			printf("\n\t\tDLN_FLUX :\t\tMB: %1.4e", RR_scatter_sols.at(i).at(j).DLN_FLUX);
	
			std::cout << std::endl;
		}
	}

	return 0;


	// -----------------------------------------------------------------------------------
	// Scattering benchmarks round-two:
	// Per: Hadded et al 1981

	// /* Set up parameters for solver --------------------------------------------------------*/
	// 
	// p = mb::BoltzmannParameters();
	// 
	// p.model = mb::ModelCode::HDGE_01;
	// p.N_terms = 10;
	// 
	// p.Nu = 10000;
	// 
	// 
	// p.initial_eV_max = 3;
	// p.USE_ENERGY_REMAP = true;		// Look for a new grid if necessary
	// p.remap_grid_trial_max = 25;				// try a new grid no more than this many times
	// p.remap_target_order_span = 10.0;			// how many orders would you prefer your EEDF to span (in eV^-3/2)
	// 
	// 
	// p.EN_Td = 25;
	// p.p_Torr = 760;
	// p.T_K = 0;
	// 
	// p.sharing = 0.5; // energy sharing between primary and secondary electrons in ionization
	// 
	// vec_ela_scattering =
	// { lib::isotropic_scattering(),  ModelB_scattering(), ModelC_scattering(), ModelD_scattering()};
	// vec_inel_scattering =
	// { lib::isotropic_scattering(),  ModelB_scattering(), ModelC_scattering(), ModelD_scattering()};
	// 
	// 
	// /* Perform Boltzmann solver calculation  */
	// 
	// // instantiate
	// //mb::BoltzmannSolver run;
	// //mb::BoltzmannOutput out;
	// 
	// 
	// 
	// mb::normal_statement("Using following parameters as starting-point for validation:");
	// p.print();
	// 
	// args_EN_strings = { "E/N = 25 Td" };
	// args_scatter_strings = { "iso-iso", "Model-B", "Model-C" , "Model-D"};
	// 
	// // vectors of ouputs
	// std::vector<mb::BoltzmannOutput> Haddad_RR_scatter_sols(vec_ela_scattering.size(), mb::BoltzmannOutput() );
	// 
	// 
	// 
	// 
	// lib::libSpeaker.printmode_normal_statements();
	// mb::mbSpeaker.printmode_normal_statements();
	// 
	// 
	// 
	// 
	// /* Develop library of analytic species -------------------------------------------------------- */
	// // not the *default* you'd expect, this paper uses different form
	// 
	// 
	// //mb::BoltzmannParameters this_p = p;
	// 
	// p.EN_Td = 25;
	// 
	// for (int j = 0; j < vec_ela_scattering.size(); ++j) {
	// 
	// 	lib::Library this_RR = lib::get_ReidRamp_Library(10e-20, 2.0, 0.516, 0.4e-20);
	// 	this_RR = lib::get_ReidRamp_Library();
	// 
	// 	//if (j >= 2) {
	// 	//	this_RR = lib::get_ReidRamp_Library(6.954e-20, 2.0, 0.516, 0.4e-20);
	// 	//}
	// 
	// 	this_RR.assign_scattering_by_type(vec_ela_scattering.at(j), lib::CollisionCode::elastic);
	// 	this_RR.assign_scattering_by_type(vec_inel_scattering.at(j), lib::CollisionCode::excitation);
	// 
	// 	mb::normal_statement("Beginning validation solution: ReidRamp, HD, E/N = " + std::to_string(p.EN_Td) + " Td " + args_scatter_strings.at(j));
	// 	run = mb::BoltzmannSolver(p, this_RR);
	// 	Haddad_RR_scatter_sols.at(j) = run.get_output();
	// 
	// }
	// 
	// 
	// 
	// 
	// 
	// 
	// //for (int i = 0; i < vec_EN_Td.size(); ++i) {
	// 
	// 	printf("\n%s", args_EN_strings.at(0).c_str());
	// 
	// 	for (int j = 0; j < vec_ela_scattering.size(); ++j) {
	// 
	// 		printf("\n\t%s", args_scatter_strings.at(j).c_str());
	// 
	// 
	// 		printf("\n\t\tkB-T :\t\tMB: %1.4e", Haddad_RR_scatter_sols.at(j).avg_en * 2.0 / 3.0);
	// 		printf("\n\t\tW_FLUX :\t\tMB: %1.4e", Haddad_RR_scatter_sols.at(j).W_FLUX);
	// 		printf("\n\t\tDT/MU :\t\tMB: %1.4e", Haddad_RR_scatter_sols.at(j).DTN_FLUX / Haddad_RR_scatter_sols.at(j).muN_FLUX);
	// 		printf("\n\t\tDL/MU :\t\tMB: %1.4e", Haddad_RR_scatter_sols.at(j).DLN_FLUX / Haddad_RR_scatter_sols.at(j).muN_FLUX);
	// 
	// 		std::cout << std::endl;
	// 
	// 		//Haddad_RR_scatter_sols.at(j).print();
	// 	}
	// //}
	// 
	// 
	// return 0;
}







