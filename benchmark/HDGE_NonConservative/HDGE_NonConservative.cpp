// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 


// This source file, when compiled, creates several analytic species and tests the program
// using the same kind of validation tables as seen in Stephens (2018)
// Compares against Reid Ramp and Lucas Salee models versus a table of known values from MCC

// these amainly function as validators for the governing equations

#include "multibolt"



int main() {


	lib::libSpeaker.printmode_normal_statements();
	mb::mbSpeaker.printmode_normal_statements();

		
	/* Set up parameters for solver --------------------------------------------------------*/

	mb::BoltzmannParameters p = mb::BoltzmannParameters();

	p.model = mb::ModelCode::HDGE;
	p.N_terms = 10;

	p.Nu = 1000;

	p.iter_max = 200;	// Force break if you haven't converged within this many iterations to avoid wasting time
	p.iter_min = 10;		// Force continue if below this number of iterations
	p.weight_f0 = 1.0;	// Iteration weight. 

	p.conv_err = 1e-10;

	p.USE_ENERGY_REMAP = true;		// Look for a new grid if necessary
	p.USE_EV_MAX_GUESS = true;

	//p.grid_trial_max = 10;				// try a new grid no more than this many times
	p.remap_target_order_span = 10.0;			// how many orders would you prefer your EEDF to span (in eV^-3/2)

	p.initial_eV_max = 100;

	p.EN_Td = 1;
	p.p_Torr = 760;
	p.T_K = 0;

	p.sharing = 0.5; // energy sharing between primary and secondary electrons in ionization




	/* Perform Boltzmann solver calculation  */

	// instantiate
	mb::BoltzmannSolver run;
	mb::BoltzmannOutput out;

	

	mb::normal_statement("Using following parameters as starting-point for validation:");
	p.print();

	

	// grid is utterly obmnoxious on #2 (starts from-0)

	std::vector < std::vector<double> > args = { {0, 0, 0},{ 0.5,0,0 }, { 1.0,0,0 }, { 0,5e-4,0.5 }, { 0, 2e-3,-0.5 } , {0, 8e-3, -1.0} };
	std::vector < std::string > args_strings = {"F=0, a=0, p=0", "F=0.5, a=0, p=0", "F=1.0, a=0, p=0", "F=0, a=5e-4, p=0.5", "F=0, a=2e-3, p=-0.5", "F=0, a=8e-3, p=-1.0" };

	// vectors of ouputs:
	std::vector<mb::BoltzmannOutput> LS_HDGE_sols(args.size(), mb::BoltzmannOutput());

	p.EN_Td = 10; // static

	int counter = 0;


	#pragma omp parallel
	#pragma omp for
	for (int counter = 0; counter < args.size(); counter++) {

		double Farg = args.at(counter)[0];
		double aarg = args.at(counter)[1];
		double parg = args.at(counter)[2];

		lib::Library LucasSalee = lib::get_LucasSalee_Library(Farg, aarg, parg);

		LucasSalee.assign_scattering_by_type(lib::isotropic_scattering(), lib::CollisionCode::elastic);
		LucasSalee.assign_scattering_by_type(lib::isotropic_scattering(), lib::CollisionCode::excitation);
		LucasSalee.assign_scattering_by_type(lib::isotropic_scattering(), lib::CollisionCode::ionization);

		mb::normal_statement("Beginning validation solution: LucasSalee, HD+GE, E/N = 10 Td : F= " + std::to_string(Farg) + ", a=" + std::to_string(aarg) + ", p=" + std::to_string(parg));
		run = mb::BoltzmannSolver(p, LucasSalee);
		LS_HDGE_sols.at(counter) = run.get_output();

	}


	/* ----------------------------------------------- */

	double N = 1e20; // fixed LS particle density

	//See: Nolan A M, Brennan M J, Ness K F, Wedding A B 1997 J. Phys. D: Appl. Phys. 30 2865-71
	// all for 10 Td
	std::vector<mb::BoltzmannOutput> Nolan_HDGE_LS_vec(args.size());
	Nolan_HDGE_LS_vec[0].avg_en = 5.57;
	Nolan_HDGE_LS_vec[0].k_iz_eff_N = 0e3 / N;
	Nolan_HDGE_LS_vec[0].W_FLUX = 7.32e4;
	Nolan_HDGE_LS_vec[0].W_BULK = 7.32e4;
	Nolan_HDGE_LS_vec[0].DTN_FLUX = 2.72e5 * N;
	Nolan_HDGE_LS_vec[0].DTN_BULK = 2.72e5 * N;
	Nolan_HDGE_LS_vec[0].DLN_FLUX = 2.66e5 * N;
	Nolan_HDGE_LS_vec[0].DLN_BULK = 2.66e5 * N;

	Nolan_HDGE_LS_vec[1].avg_en = 5.22;
	Nolan_HDGE_LS_vec[1].k_iz_eff_N = 1.33e3 / N;
	Nolan_HDGE_LS_vec[1].W_FLUX = 7.32e4;
	Nolan_HDGE_LS_vec[1].W_BULK = 8.58e4;
	Nolan_HDGE_LS_vec[1].DTN_FLUX = 2.55e5 * N;
	Nolan_HDGE_LS_vec[1].DTN_BULK = 2.73e5 * N;
	Nolan_HDGE_LS_vec[1].DLN_FLUX = 2.49e5 * N;
	Nolan_HDGE_LS_vec[1].DLN_BULK = 2.85e5 * N;

	Nolan_HDGE_LS_vec[2].avg_en = 4.97;
	Nolan_HDGE_LS_vec[2].k_iz_eff_N = 2.42e3 / N;
	Nolan_HDGE_LS_vec[2].W_FLUX = 7.32e4;
	Nolan_HDGE_LS_vec[2].W_BULK = 9.47e4;
	Nolan_HDGE_LS_vec[2].DTN_FLUX = 2.43e5 * N;
	Nolan_HDGE_LS_vec[2].DTN_BULK = 2.72e5 * N;
	Nolan_HDGE_LS_vec[2].DLN_FLUX = 2.38e5 * N;
	Nolan_HDGE_LS_vec[2].DLN_BULK = 2.94e5 * N;

	Nolan_HDGE_LS_vec[3].avg_en = 5.44;
	Nolan_HDGE_LS_vec[3].k_iz_eff_N = -1.62e3 / N;
	Nolan_HDGE_LS_vec[3].W_FLUX = 7.33e4;
	Nolan_HDGE_LS_vec[3].W_BULK = 7.02e4;
	Nolan_HDGE_LS_vec[3].DTN_FLUX = 2.67e5 * N;
	Nolan_HDGE_LS_vec[3].DTN_BULK = 2.61e5 * N;
	Nolan_HDGE_LS_vec[3].DLN_FLUX = 2.61e5 * N;
	Nolan_HDGE_LS_vec[3].DLN_BULK = 2.64e5 * N;

	Nolan_HDGE_LS_vec[4].avg_en = 5.56;
	Nolan_HDGE_LS_vec[4].k_iz_eff_N = -1.19e3 / N;
	Nolan_HDGE_LS_vec[4].W_FLUX = 7.32e4;
	Nolan_HDGE_LS_vec[4].W_BULK = 7.32e4;
	Nolan_HDGE_LS_vec[4].DTN_FLUX = 2.73e5 * N;
	Nolan_HDGE_LS_vec[4].DTN_BULK = 2.73e5 * N;
	Nolan_HDGE_LS_vec[4].DLN_FLUX = 2.66e5 * N;
	Nolan_HDGE_LS_vec[4].DLN_BULK = 2.66e5 * N;

	Nolan_HDGE_LS_vec[5].avg_en = 5.73;
	Nolan_HDGE_LS_vec[5].k_iz_eff_N = -2.60e3 / N;
	Nolan_HDGE_LS_vec[5].W_FLUX = 7.32e4;
	Nolan_HDGE_LS_vec[5].W_BULK = 7.55e4;
	Nolan_HDGE_LS_vec[5].DTN_FLUX = 2.79e5 * N;
	Nolan_HDGE_LS_vec[5].DTN_BULK = 2.86e5 * N;
	Nolan_HDGE_LS_vec[5].DLN_FLUX = 2.73e5 * N;
	Nolan_HDGE_LS_vec[5].DLN_BULK = 2.64e5 * N;


	//See: Raspopovic Z M, Sakadzic S, Bzenic S A and Petrovic Z Lj 1999 IEEE Trans. Plasma Sci. 27 1241
	// all for 10 Td
	std::vector<mb::BoltzmannOutput> Raspopovic_HDGE_LS_vec(args.size());

	Raspopovic_HDGE_LS_vec[0].avg_en = 5.57;
	Raspopovic_HDGE_LS_vec[0].k_iz_eff_N = 0e3 / N;
	Raspopovic_HDGE_LS_vec[0].W_FLUX = 7.28e4;
	Raspopovic_HDGE_LS_vec[0].W_BULK = 7.32e4;
	Raspopovic_HDGE_LS_vec[0].DTN_FLUX = 2.71e5 * N;
	Raspopovic_HDGE_LS_vec[0].DTN_BULK = 2.73e5 * N;
	Raspopovic_HDGE_LS_vec[0].DLN_FLUX = 2.65e5 * N;
	Raspopovic_HDGE_LS_vec[0].DLN_BULK = 2.64e5 * N;

	Raspopovic_HDGE_LS_vec[1].avg_en = 5.22;
	Raspopovic_HDGE_LS_vec[1].k_iz_eff_N = 1.33e3 / N;
	Raspopovic_HDGE_LS_vec[1].W_FLUX = 7.31e4;
	Raspopovic_HDGE_LS_vec[1].W_BULK = 8.58e4;
	Raspopovic_HDGE_LS_vec[1].DTN_FLUX = 2.56e5 * N;
	Raspopovic_HDGE_LS_vec[1].DTN_BULK = 2.73e5 * N;
	Raspopovic_HDGE_LS_vec[1].DLN_FLUX = 2.49e5 * N;
	Raspopovic_HDGE_LS_vec[1].DLN_BULK = 2.85e5 * N;

	Raspopovic_HDGE_LS_vec[2].avg_en = 4.97;
	Raspopovic_HDGE_LS_vec[2].k_iz_eff_N = 2.42e3 / N;
	Raspopovic_HDGE_LS_vec[2].W_FLUX = 7.30e4;
	Raspopovic_HDGE_LS_vec[2].W_BULK = 9.51e4;
	Raspopovic_HDGE_LS_vec[2].DTN_FLUX = 2.42e5 * N;
	Raspopovic_HDGE_LS_vec[2].DTN_BULK = 2.72e5 * N;
	Raspopovic_HDGE_LS_vec[2].DLN_FLUX = 2.37e5 * N;
	Raspopovic_HDGE_LS_vec[2].DLN_BULK = 2.93e5 * N;

	Raspopovic_HDGE_LS_vec[3].avg_en = 5.45;
	Raspopovic_HDGE_LS_vec[3].k_iz_eff_N = -1.60e3 / N;
	Raspopovic_HDGE_LS_vec[3].W_FLUX = 7.30e4;
	Raspopovic_HDGE_LS_vec[3].W_BULK = 7.01e4;
	Raspopovic_HDGE_LS_vec[3].DTN_FLUX = 2.67e5 * N;
	Raspopovic_HDGE_LS_vec[3].DTN_BULK = 2.63e5 * N;
	Raspopovic_HDGE_LS_vec[3].DLN_FLUX = 2.60e5 * N;
	Raspopovic_HDGE_LS_vec[3].DLN_BULK = 2.66e5 * N;

	Raspopovic_HDGE_LS_vec[4].avg_en = 5.57;
	Raspopovic_HDGE_LS_vec[4].k_iz_eff_N = -1.17e3 / N;
	Raspopovic_HDGE_LS_vec[4].W_FLUX = 7.29e4;
	Raspopovic_HDGE_LS_vec[4].W_BULK = 7.34e4;
	Raspopovic_HDGE_LS_vec[4].DTN_FLUX = 2.72e5 * N;
	Raspopovic_HDGE_LS_vec[4].DTN_BULK = 2.74e5 * N;
	Raspopovic_HDGE_LS_vec[4].DLN_FLUX = 2.65e5 * N;
	Raspopovic_HDGE_LS_vec[4].DLN_BULK = 2.64e5 * N;


	Raspopovic_HDGE_LS_vec[5].avg_en = arma::datum::nan;
	Raspopovic_HDGE_LS_vec[5].k_iz_eff_N = arma::datum::nan;
	Raspopovic_HDGE_LS_vec[5].W_FLUX = arma::datum::nan;
	Raspopovic_HDGE_LS_vec[5].W_BULK = arma::datum::nan;
	Raspopovic_HDGE_LS_vec[5].DTN_FLUX = arma::datum::nan;
	Raspopovic_HDGE_LS_vec[5].DTN_BULK = arma::datum::nan;
	Raspopovic_HDGE_LS_vec[5].DLN_FLUX = arma::datum::nan;
	Raspopovic_HDGE_LS_vec[5].DLN_BULK = arma::datum::nan;



	/*   ------    */

	std::cout << std::endl;

	for (int i = 0; i < args.size(); ++i) {

		


		printf("\n%s", args_strings.at(i).c_str());


		double elemmb = LS_HDGE_sols[i].avg_en;
		double elemref1 = Nolan_HDGE_LS_vec[i].avg_en;
		double elemref2 = Raspopovic_HDGE_LS_vec[i].avg_en;

		double err1 = (elemmb - elemref1) / abs(elemmb);
		double err2 = (elemmb - elemref2) / abs(elemmb);
		printf("\n\tavg_en :\t\tMB: %1.4e\t\tMC: %1.4e\t\tMC: %1.4e\t\t%1.4e%%\t\t%1.4e%%", elemmb, elemref1, elemref2, err1*100, err2*100);


		elemmb = LS_HDGE_sols[i].k_iz_eff_N;
		elemref1 = Nolan_HDGE_LS_vec[i].k_iz_eff_N;
		elemref2 = Raspopovic_HDGE_LS_vec[i].k_iz_eff_N;

		err1 = (elemmb - elemref1) / abs(elemmb);
		err2 = (elemmb - elemref2) / abs(elemmb);
		printf("\n\tk_iz_eff_N :\t\tMB: %1.4e\t\tMC: %1.4e\t\tMC: %1.4e\t\t%1.4e%%\t\t%1.4e%%", elemmb, elemref1, elemref2, err1 * 100, err2 * 100);


		elemmb = LS_HDGE_sols[i].W_FLUX;
		elemref1 = Nolan_HDGE_LS_vec[i].W_FLUX;
		elemref2 = Raspopovic_HDGE_LS_vec[i].W_FLUX;

		err1 = (elemmb - elemref1) / abs(elemmb);
		err2 = (elemmb - elemref2) / abs(elemmb);
		printf("\n\tW_FLUX :\t\tMB: %1.4e\t\tMC: %1.4e\t\tMC: %1.4e\t\t%1.4e%%\t\t%1.4e%%", elemmb, elemref1, elemref2, err1 * 100, err2 * 100);


		elemmb = LS_HDGE_sols[i].W_BULK;
		elemref1 = Nolan_HDGE_LS_vec[i].W_BULK;
		elemref2 = Raspopovic_HDGE_LS_vec[i].W_BULK;

		err1 = (elemmb - elemref1) / abs(elemmb);
		err2 = (elemmb - elemref2) / abs(elemmb);
		printf("\n\tW_BULK :\t\tMB: %1.4e\t\tMC: %1.4e\t\tMC: %1.4e\t\t%1.4e%%\t\t%1.4e%%", elemmb, elemref1, elemref2, err1 * 100, err2 * 100);



		elemmb = LS_HDGE_sols[i].DTN_FLUX;
		elemref1 = Nolan_HDGE_LS_vec[i].DTN_FLUX;
		elemref2 = Raspopovic_HDGE_LS_vec[i].DTN_FLUX;

		err1 = (elemmb - elemref1) / abs(elemmb);
		err2 = (elemmb - elemref2) / abs(elemmb);
		printf("\n\tDTN_FLUX :\t\tMB: %1.4e\t\tMC: %1.4e\t\tMC: %1.4e\t\t%1.4e%%\t\t%1.4e%%", elemmb, elemref1, elemref2, err1 * 100, err2 * 100);

		elemmb = LS_HDGE_sols[i].DTN_BULK;
		elemref1 = Nolan_HDGE_LS_vec[i].DTN_BULK;
		elemref2 = Raspopovic_HDGE_LS_vec[i].DTN_BULK;

		err1 = (elemmb - elemref1) / abs(elemmb);
		err2 = (elemmb - elemref2) / abs(elemmb);
		printf("\n\tDTN_BULK :\t\tMB: %1.4e\t\tMC: %1.4e\t\tMC: %1.4e\t\t%1.4e%%\t\t%1.4e%%", elemmb, elemref1, elemref2, err1 * 100, err2 * 100);

		elemmb = LS_HDGE_sols[i].DLN_FLUX;
		elemref1 = Nolan_HDGE_LS_vec[i].DLN_FLUX;
		elemref2 = Raspopovic_HDGE_LS_vec[i].DLN_FLUX;

		err1 = (elemmb - elemref1) / abs(elemmb);
		err2 = (elemmb - elemref2) / abs(elemmb);
		printf("\n\tDLN_FLUX :\t\tMB: %1.4e\t\tMC: %1.4e\t\tMC: %1.4e\t\t%1.4e%%\t\t%1.4e%%", elemmb, elemref1, elemref2, err1 * 100, err2 * 100);

		elemmb = LS_HDGE_sols[i].DLN_BULK;
		elemref1 = Nolan_HDGE_LS_vec[i].DLN_BULK;
		elemref2 = Raspopovic_HDGE_LS_vec[i].DLN_BULK;

		err1 = (elemmb - elemref1) / abs(elemmb);
		err2 = (elemmb - elemref2) / abs(elemmb);
		printf("\n\tDLN_BULK :\t\tMB: %1.4e\t\tMC: %1.4e\t\tMC: %1.4e\t\t%1.4e%%\t\t%1.4e%%", elemmb, elemref1, elemref2, err1 * 100, err2 * 100);

		std::cout << std::endl;

	}

	





	return 0;
}







