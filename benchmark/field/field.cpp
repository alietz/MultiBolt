// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 


// Source file that checks electric field influence on the gas is working
// Zero temperature with non-zero field and constant elastic xsec should
// create a Druyvesteyn distribution

#include "multibolt"



int main() {


	lib::libSpeaker.printmode_normal_statements();
	mb::mbSpeaker.printmode_normal_statements();



	/* Develop library of analytic species -------------------------------------------------------- */

	lib::Library lib = lib::get_ReidRamp_Library();
	
	double MRATIO = lib.allspecies[0]->allcollisions[0]->Mratio();
	double SIGMA = lib.allspecies[0]->allcollisions[0]->eval_at_eV(1); // should be constant

	// get rid of excited-state for this case
	lib.erase_by_index(1);
	lib.allspecies[0]->erase_by_index(1); // get rid of the neutral excitation
	
	/* Set up parameters for solver --------------------------------------------------------*/

	mb::BoltzmannParameters p = mb::BoltzmannParameters();

	p.model = mb::ModelCode::HD;
	p.N_terms = 10; 

	p.Nu = 1000;	

	p.iter_max = 100;	// Force break if you haven't converged within this many iterations to avoid wasting time
	p.iter_min = 4;		// Force continue if below this number of iterations
	p.weight_f0 = 1.0;	// Iteration weight. 


	p.initial_eV_max = 50;

	arma::colvec EN_vec = arma::logspace(-3, 3, 10);
	p.EN_Td = 0;
	p.p_Torr = 760;

	p.T_K = 0;

	p.sharing = 0.5; // energy sharing between primary and secondary electrons in ionization

	p.USE_ENERGY_REMAP = true;
	p.USE_EV_MAX_GUESS = true;

	std::vector<mb::BoltzmannSolver> run_vec(EN_vec.size(), mb::BoltzmannSolver());
	std::vector<mb::BoltzmannOutput> sols_vec(EN_vec.size(), mb::BoltzmannOutput());


	/* Perform Boltzmann solver calculation  */

	// instantiate and perform BE solutions
	#pragma omp parallel
	#pragma omp for
	for (int i = 0; i < EN_vec.size(); ++i) {
		p.EN_Td = EN_vec(i);
		run_vec.at(i) = mb::BoltzmannSolver(p, lib);
		sols_vec.at(i) = run_vec.at(i).get_output();
	}



	for (int i = 0; i < EN_vec.size(); ++i) {
		printf("\nE/N: %2.2f Td", EN_vec.at(i));

		auto eV = sols_vec.at(i).eV_even;
		auto calc_druyvesteyn = sols_vec.at(i).f0;

		arma::colvec f_Druy = arma::exp(-arma::pow(2.0 * eV * mb::QE / mb::ME, 2.0) /
			(4.0 / 3.0 / MRATIO * pow(mb::QE / mb::ME * run_vec.at(i).p.EN_Td * 1e-21 / SIGMA, 2.0)));
		f_Druy = f_Druy / arma::as_scalar(trapz(eV, sqrt(eV) % f_Druy));

		// Mean electron energy
		double elem_calc = arma::as_scalar(trapz(eV, pow(eV, 1.5) % calc_druyvesteyn));
		double elem_exact = arma::as_scalar(trapz(eV, pow(eV, 1.5) % f_Druy));


		double err1 = (elem_calc - elem_exact) / abs(elem_calc);
		printf("\n\tMean Energy :\t\tMB: %1.4e\t\tExact: %1.4e\t\tt%1.4e%%", elem_calc, elem_exact, err1 * 100);

	}


	


	return 0;
}







