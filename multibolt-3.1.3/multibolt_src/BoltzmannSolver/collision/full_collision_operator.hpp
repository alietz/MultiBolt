// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"

// Apply all elements of the collision operator on their own terms

// todo: could afford to move the per-k and per-gas for-loops in here, might speed things up
void mb::BoltzmannSolver::full_collision_operator(arma::sword ell, arma::SpMat<double>& A) {


	C_ela(ell, A);

	C_exc(ell, A);

	C_att(ell, A);

	C_iz(ell, A);

	C_sup(ell, A);

	return;
}