// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"


// The elastic contribution to the collision operator
void mb::BoltzmannSolver::C_ela(arma::sword ell, arma::SpMat<double>& A) {

	for (auto set : { g.i_elastic, g.i_effective }) {
		for (auto i_x : set) {

			auto xsec = g.xsec_ptr(i_x);
			auto spec = g.species_ptr(g.i_collision_species(i_x));

			double frac = g.i_frac(i_x);
			if (mb::doubles_are_same(frac, 0)) { continue; }

			mb::debug_statement("Applying C_ela[" + std::to_string(ell) + "] with process " + xsec->process() + " in species " + spec->name());

			for (arma::sword k = 0; k < present_Nu; ++k) {


				double u = 0;
				double elasticMT_ICS = 0;
				double Du = 0;
				double total_ICS = 0;

				// -----------------------------------------------------
				// Non-zero momentum transfer term for ell == 0
				if (ell == 0) {

					// Uses forward difference

					// for ell=0, anisotropic scattering effects are all captured by the MT Xsec
					// emphasis: the MT xsec is *defined* to be in this place, it is not here due to some kind of scattering approximation

					// f0(k)
					u = g.u_e(k);
					elasticMT_ICS = g.sigma_e(k, i_x); 
					Du = Du_at_u(u);

					if (xsec->code() == lib::CollisionCode::effective) {
						elasticMT_ICS = elasticMT_ICS - species_total_inelastic(u / mb::QE, g.i_collision_species(i_x)); // adjust down
					}

					A.at((present_Nu * ell) + k, (present_Nu * ell) + k) +=
						-1 * frac * 2.0 * (xsec->Mratio()) / Du * (u * u) * Np * elasticMT_ICS;

					if (k + 1 < present_Nu) {
						// f0(k + 1)
						u = g.u_e(k + 1);
						elasticMT_ICS = g.sigma_e(k + 1, i_x);
						Du = Du_at_u(u);

						A.at((present_Nu * ell) + k, (present_Nu * ell) + k + 1) +=
							frac * 2.0 * (xsec->Mratio()) / Du * (u * u) * Np * elasticMT_ICS;
					}
				}

				// -----------------------------------------------------
				// Lorentz approximation term for ell > 0

				if (is_even_or_zero(ell)) {
					u = g.u_e(k);
					elasticMT_ICS = g.sigma_e(k, i_x);
					Du = Du_at_u(u);

					if (xsec->code() == lib::CollisionCode::effective) {
						elasticMT_ICS = elasticMT_ICS - species_total_inelastic(u / mb::QE, g.i_collision_species(i_x)); // adjust down
					}
				}
				else {
					u = g.u_o(k);
					elasticMT_ICS = g.sigma_o(k, i_x);
					Du = Du_at_u(u);
					if (xsec->code() == lib::CollisionCode::effective) {
						elasticMT_ICS = elasticMT_ICS - species_total_inelastic(u / mb::QE, g.i_collision_species(i_x)); // adjust down
					}
				}
				total_ICS = xsec->scattering()->ratio_elasticTotal_to_elasticMT(u / mb::QE, u / mb::QE) * elasticMT_ICS;
				double Phi = xsec->scattering()->scattering_integral(u / mb::QE, u / mb::QE, ell);

				// for anisotropic scattering, this term actually requires the total xsec rather than the MT
				A.at((ell * present_Nu) + k, (ell * present_Nu) + k) +=
					frac * u * Np * (Phi - 1) * total_ICS;

			}


			// -- temperature dependence, Chapman-Cowling correction 
			// this expression is relying on the product rule expansion
			// term 1: 2 * u * sigma * df_du
			// term 2: u^2 * dsigma_du * df_du
			// term 3: u^2 * sigma * d2f_du2
			if (ell == 0 && !COLD_CASE) {

				arma::colvec elasticMT_vec = g.sigma_e.col(i_x);

				// adjust for effective
				if (xsec->code() == lib::CollisionCode::effective) {
					elasticMT_vec = elasticMT_vec - species_total_inelastic(g.u_e / mb::QE, g.i_collision_species(i_x)); // adjust down
				}


				arma::colvec d_elasticMT_du_vec(present_Nu, arma::fill::zeros);

				//d_elasticMT_du_vec(g.t_idx) = (elasticMT_vec(g.t_idx) - (elasticMT_vec(g.idx_t))) / 2.0 / g.Du; // <- CAUSES ERROR DUE TO MIS-SIZED SIGMA

				d_elasticMT_du_vec(g.idx_t) = (elasticMT_vec(g.t_idx) - (elasticMT_vec(g.idx_t))) / Du_at_u(g.u_e(g.idx_t));// (g.u_e(g.t_idx) - g.u_e(g.idx_t));
				d_elasticMT_du_vec(0) = (elasticMT_vec(1) - elasticMT_vec(0)) / Du_at_u(g.u_e(0)); //(g.u_e(1) - g.u_e(0));
				d_elasticMT_du_vec(present_Nu - 1) = (elasticMT_vec(present_Nu - 1) - elasticMT_vec(present_Nu - 2)) / Du_at_u(g.u_e(present_Nu - 1)); //(g.u_e(present_Nu - 1) - g.u_e(present_Nu - 2));




				for (arma::sword k = 0; k < present_Nu; ++k) {

					double FACTOR = frac * 2.0 * (xsec->Mratio()) * Np * (mb::KB * p.T_K);

					// apply first derivatives kernal
					arma::colvec coeffsForward = { -3.0 / 2.0, 2.0, -1.0 / 2.0 }; // worked! // Second-order accuracy forward difference
					//arma::colvec coeffsForward = { -11.0 / 6.0, 3.0, -3.0 / 2.0, 1.0 / 3.0 }; // third-order accuracy
					//arma::colvec coeffsForward = { -25.0 / 12.0, 4.0, -3.0, 4.0 / 3.0, -1.0 / 4.0 }; // fourth-order accuracy
					coeffsForward = coeffsForward * -1;

					for (arma::sword i = 0; i < arma::sword(coeffsForward.n_elem); ++i) {

						double c = coeffsForward(i);
						auto idx = k + i;

						if (idx >= 0 && idx < present_Nu) {

							double u = g.u_e(idx);
							double elasticMT_ICS = elasticMT_vec(idx);
							double d_DCS_du = d_elasticMT_du_vec(idx);
							double Du = Du_at_u(u); //

							A(k, idx) += (c / Du) * FACTOR * (2 * u) * elasticMT_ICS
								+ (c / Du) * FACTOR * (u * u) * d_DCS_du
								+ (c / Du) * FACTOR * (u * u) * elasticMT_ICS;

						}

					}



					// apply second derivatives kernal
					arma::colvec coeffsCentral = { -1.0 / 560, 8.0 / 315.0, -1.0 / 5.0, 8.0 / 5.0, -205.0 / 72.0, 8.0 / 5.0, -1.0 / 5.0, 8.0 / 315.0, -1.0 / 560.0 }; // eigth-order accuracy, central second deriviatve
					//arma::colvec coeffsCentral = { 1.0, -2.0, 1.0 }; // first-order accuracy, central second deriviatve

					for (arma::sword i = 0; i < arma::sword(coeffsCentral.n_elem); ++i) {
						double c = coeffsCentral(i);
						auto idx = k + i - (coeffsCentral.n_elem - 1) / 2;

						if (idx >= 0 && idx < present_Nu) {
							double u = g.u_e(idx);
							double elasticMT_ICS = elasticMT_vec(idx);
							double d_DCS_du = d_elasticMT_du_vec(idx);
							double Du = Du_at_u(u); //

							A(k, idx) += (c / Du / Du) * FACTOR * (u * u) * elasticMT_ICS;

						}
					}
				}
			}
		}
	}
	
}


