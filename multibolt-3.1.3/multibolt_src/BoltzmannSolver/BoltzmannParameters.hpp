// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include"multibolt"


// A BoltzmannParameters object is the struct-like
// container for settings that MultiBolt cares about when running, printing, etc.
// Must be passed to BoltmannSolver to execute a run.


class BoltzmannParameters  {

	public:

		std::vector<std::string> argslist; // if MultiBolt was called with arguments, copy those arguments here

		int Nu = 1000; // grid points
		int N_terms = 6; // expansion terms
		
		mb::ModelCode model = mb::ModelCode::HDGE; // hydrodynamic, hydro+gradient-expansion, or steady-state-townsend


		bool USE_ENERGY_REMAP = false; // activate/disable grid remapping
	
		bool USE_EV_MAX_GUESS = false; // empiric guess: say you start eV_max = E/N [Td]

		bool USE_NU_REMAP = false; // allows for automatic increasing of present_Nu if a negative f0 is found 

		// 11/10/2022
		bool DONT_ENFORCE_SUM = false; // If yes, allow species fractions to sum to something other than 1
		// this is useful for *some* excited state modeling

		int iter_max = 100; // allowable iterations per scheme
		int iter_min = 10;
		int remap_Nu_max = 2000; // maximum number Nu for remapping grid 
		int remap_Nu_increment = 200; // number Nu increases by per iteration for remapping grid

		double conv_err = 1e-6; // convergence error target for the interable coefficient

		double initial_eV_max = 100; // roof of grid considered - rule of thumb, this should be high enough such that the EEDF tail is ~10 orders of magnitude down from the head

		double p_Torr = 760; // pressure [Torr] - used for calculating Np
		double T_K = 300; // temperature [T_K] - thermal dependence 

		double EN_Td = 100; // density reduled electric field strength in Td=1e-21 Vm^2

		int remap_grid_trial_max = 10; // try a new grid no more than this many times

		double remap_target_order_span = 10.0; // how many orders would you prefer your EEDF to span (in eV^-3/2)

		double weight_f0 = 1.0; // non-conservative swarms (iz + att)

		double sharing = 0.5; // energy split between the primary and secondary. 0.5 means equal sharing, 0 means one-takes-all. Use in range [0 and 0.5].

		bool SHY = false; // allows early quit-out when results might be non-physical

		lib::InterpolationMethod interp_method = lib::InterpolationMethod::Linear;

		void print() {

			mb::normal_statement(print_str());
		};

		std::string print_str() {
			std::stringstream ss; ss.str("");
			ss.setf(std::ios_base::scientific, std::ios_base::floatfield);
			ss << "\n";
			ss << "\tN points: " << Nu << "\n";
			ss << "\tN terms: " << N_terms << "\n";
			ss << "\tModel: " << mb::model_map.find(model)->second << "\n";
			ss << "\tUsing 'Energy Remap'?: " << std::boolalpha << USE_ENERGY_REMAP << "\n";
			ss << "\tUse eV-max guess?: " << std::boolalpha << USE_EV_MAX_GUESS << "\n";
			ss << "\tUsing 'Nu Remap'?: " << std::boolalpha << USE_NU_REMAP << "\n";

			ss << "\titer_min: " << iter_min << "\n";
			ss << "\titer_max: " << iter_max << "\n";

			ss << "\tConv. Err: " << std::setprecision(4) << conv_err << "\n";
			ss << "\tMax eV: " << std::setprecision(4) << initial_eV_max << "\n";

			ss << "\tPressure [torr]: " << std::setprecision(4) << p_Torr << "\n";
			ss << "\tTemp. [K]: " << std::setprecision(4) << T_K << "\n";

			ss << "\tE/N [Td]: " << std::setprecision(4) << EN_Td << "\n";

			ss << "\tMax grid trials: " << remap_grid_trial_max << "\n";
			ss << "\tMax remap Nu: " << remap_Nu_max << "\n";
			ss << "\tRemap Nu incremenmt: " << remap_Nu_increment << "\n";
			ss << "\tEEDF Order Span (target): " << std::setprecision(4) << remap_target_order_span << "\n";
			ss << "\tWeight (f0): " << std::setprecision(4) << weight_f0 << "\n";

			ss << "\tIonization energy sharing: " << std::setprecision(4) << sharing << "\n";

			ss << "\tUsing SHY mode?: " << std::boolalpha << SHY << "\n";

			ss << "\tEnforcing 'sum(species)=1'?: " << std::boolalpha << !(DONT_ENFORCE_SUM) << "\n";
			
			ss << "\tInterpolation Method: " << lib::interp_map.find(interp_method)->second << "\n";

			return ss.str();
		};
};

