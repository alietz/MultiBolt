// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#include "multibolt"


// Whether or not print should be allowed to happen
bool print_err_is_allowed() {
	if (mb::mbSpeaker.get_outOp() != mb::OutputOption::SILENT) {
		return true;
	}
	return false;
}


void grid_nan_ocurred() {

	std::string str = "Gridding procedure failed such that nan exists in at least one Xsec. Solution cannot continue.";

	if (true /*lib::print_err_is_allowed()*/) {
		lib::error_statement(str);
	}

	throw std::runtime_error(str);
}



void throw_err_statement(std::string str) {

	lib::error_statement(str);

	throw std::runtime_error(str);

}