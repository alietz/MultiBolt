﻿# MultiBolt

Current version: multibolt-3.1.3


MultiBolt is a multi-term Boltzmann equation solver for low temperature plasma applications which solves the Boltzmann equation for models in hydrodynamic ("Pulsed-Townsend-like") and steady-state Townsend conditions. MultiBolt can calculate the electron energy distribution functions in electron swarms as well as the rate, transport, and growth coefficients.

This is a C++ header library refactoring of MultiBolt (originally in MATLAB) with new functionality and modeling capabilities. See 'CHANGELOG' for details.

# Electron Collision Cross Sections

MultiBolt can read LXCat-formatted electron collision cross sections (see 'cross-sections' for a selection included in this distribution). You may find these at https://nl.lxcat.net/

MultiBolt can use cross sections both as-read from these data files and as user-defined. See the scripts in 'multibolt/validation/' for an example of this in-practice used for model (“ReidRamp” and “LucasSalee”) cross sections.

# License
MultiBolt is open source and permissively licensed (MIT). We impose no restriction on the open-or-closedness of derivative works.
In short, all redistributions of MultiBolt source code and binaries are allowed given that:
* The copyright is maintained.
* The license is kept present (see 'LICENSE').

You may find a copy of the MIT license at https://opensource.org/licenses/MIT

Redistributions of MultiBolt *source code* might include redistributions of its likewise open-source dependencies (Armadillo, SuperLU 5.2, etc.). As a reminder, you must obey the instructions of these dependencies and include their license files as is appropriate.

# Citing MultiBolt
If you use MultiBolt in your work, please cite the corresponding publications (see 'multibolt/docs'):

* Stephens, J. (2018). A multi-term Boltzmann equation benchmark of electron-argon cross-sections for use in low temperature plasma models. _Journal of Physics D: Applied Physics_, _51_(12), 125203.

* Flynn, M., Neuber, A., & Stephens, J. (2021). Benchmarking the calculation of electrically insulating properties of complex gas mixtures using a multi-term Boltzmann equation model. _Journal of Physics D: Applied Physics_, _55_(1), 015201.

# Documentation

Please peruse the documentation ('/multibolt/docs/') and assosciated publications (also in 'multibolt/docs') before getting started.

An exhaustive list of equations as implemented in the code is available in MultiBolt_doc*.pdf.

Please see '/multibolt/docs/' for instructions on using the terminal argument-driven pre-compiled binary (on Windows, 'multibolt/bin/multibolt_win64.exe'). For quick reference, execute with the argument '--help' to print a list of arguments with instructions.



Best wishes
Max Flynn, Jacob Stephens
TTU

----

# Getting Started

Instructions date: 03/28/2022

## Out-of-the-box

An executable of MultiBolt as a terminal-argument driven application is pre-compiled and provided here for the sake of the user. For its source code, please see 'multibolt/applications/command/'.

This version is recommended for "simple" use-cases. In broad terms, these cases include:
* Sweeps of a single independent variable.
    * Currently allows for sweeps in: Reduced field strength ("EN_Td"), temperature ("T_K"), binary-gas fractions ("bin_frac"), grid resolution ("Nu"), and number of expansion terms ("N_terms").
* Calculations using constant, tabulated cross sections.
* Analytic scattering treatments.

Scripts which take advantage of this version of MultiBolt may be found in 'multibolt/bin/script_examples'. Currently, only batch scripts for Windows are provided.

Some arguments include path and file names, which might include spaces. You may wrap all arguments in quotes to avoid issues with this. For example:

    --export_location "My Path With Spaces In its Name"

### Windows

Please see 'multibolt/bin/multibolt_win64.exe'. From anywhere on your machine, you may execute it by using:

    multibolt_bin_dir/multibolt_win64.exe [arguments]

### Linux

Please see 'multibolt/bin/multibolt_linux'. From anywhere on your machine, you may execute it by using:

    multibolt_bin_dir/multibolt_linux [arguments]
    
You may need to ensure 'multibolt_linux' has execution permissions by using:

    chmod u+x multibolt_bin_dir/multibolt_linux

### GUI

A python+PyQt5 graphical-user interface is distributed alongside MultiBolt. This GUI handles the calling of terminal arguments. Please see 'multibolt/GUI/MultiBoltGUI.exe' (Windows only) to get started.

On both Windows and Linux, MultiBoltGUI may be used or compiled-anew from the source .py scripts. See 'multibolt/GUI/README.md' and 'multibolt/GUI/src/' for details.

## Building MultiBolt

For more flexible work not accounted for by the previous section, development, bug-testing, or more-seamless integration with other C++ programs, MultiBolt may be compiled from scratch as a part of a C++ program.

At its core, MultiBolt is a header-only library with some dependencies. A C++ programmer may include:
    
    #include <multibolt>
    
at the top of their work to use it. At time of writing, the best examples of MultiBolt source code may be found in 'multibolt/bin/script_examples/source_equivalent'. You may use these as a reference while writing your own.

Please familiarize yourself with the configuration header file "multibolt/multibolt-*/multibolt_src/config.hpp" before getting started. This is, for instance, where you may hard-disable MultiBolt's use of OpenMP.

### Dependencies

#### Armadillo

You must initialize the Armadillo submodule by using, at the top-level of the repository,

    git submodule init
    git submodule update --recursive
    
Alternatively, ensure a whole armadillo repository is copied/unzipped to "multibolt/dependencies/include/armadillo/armadillo_src/". An archive of armadillo-10.8 is provided in "multibolt/dependencies/include/armadillo/armadillo_archive/" for safety's sake.

To use a version of Armadillo already on your machine, see "Building with CMake" and "Building with Visual Studio" below.

#### SuperLU

Armadillo uses SuperLU 5.2 to solve sparse matrices. Static libraries for both Windows ("superlu_5.2.2_win64.lib") and Linux ('libsuperlu.a') are provided in "multibolt/dependencies/lib/SuperLU/SuperLU_build".

To build your own copy of SuperLU, please refer to the instructions in SuperLU's own repository. Follow the submodule instructions per-armadillo to make this available in "multibolt/dependencies/lib/SuperLU/SuperLU_src"

To use a version of SuperLU 5.2 already on your machine, see "Building with CMake" and "Building with Visual Studio" below.

#### BLAS

Armadillo uses a BLAS library for some matrix operations. Static libraries for both Windows ("openblas_0.3.19_win64.lib") and Linux ('libblas.a') are provided in "multibolt/dependencies/lib/BLAS/".

To build your own copy of BLAS, please refer to the instructions per OpenBLAS or SuperLU's own repository (see above); SuperLU has an option to compile a BLAS library during its own compilation.

To use a version of BLAS or OpenBLAS already on your machine, see "Building with CMake" and "Building with Visual Studio" below.

### Building using CMake

At time of writing, building with CMake is also confirmed to work on both Linux and Windows 
(using MSVC). For best results, use CMake-GUI to control configuration options.

Out-of-source building is highly reccomended.
    
To use versions of dependencies already on your machine outside of the respository, edit the highlighted section in the toplevel "CMakeLists.txt".

### Building using Visual Studio

A Visual Studio project structure is provided for Windows users. Simply open 'multibolt/visualstudio/MultiBolt_vs.sln'to get started.

Built files will be written solely within "multibolt/visualstudio/"

For batch builds, be sure to disable the building of x32 bit projects: this is incompatible with MultiBolt's dependencies.

To use versions of dependencies already on your machine outside of the respository, edit the project properties inside Visual Studio by navigating:

    Project > Properties > C/C++ > General > Additional Include Directories
    Project > Properties > Linker > General > Additional Library Directories
    Project > Properties > Linker > General > Additional Dependencies
    
    and changing the entries already available in the highlighted fields.

## Notes about building with OpenMP

MultiBolt will use OpenMP when available. At time of writing, MultiBolt in-general will only use OpenMP to solve parallel orders of gradient expansion simultaneously. In the case of "command.cpp" (the terminal-argument source), whole calculations of sweeps will be solved in parallel. For parallelization in your own work, reference to 'multibolt/applications/command/command.cpp' may be useful.

