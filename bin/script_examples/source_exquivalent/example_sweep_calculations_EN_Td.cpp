// MultiBolt v3.1.1
// Example script: read LXCat cross section files for collisions and
// and set source to run multiple calculations, sweeping over values of EN_Td
// then export the output as files.


#include "multibolt"
#include <vector>

int main() {


	mb::mbSpeaker.printmode_normal_statements();




	/* Collision Library Definition */

	// The Xsec files that have the collisions we are interested in
	std::vector<std::string> Xsec_fids = { "../../../../../../cross-sections/Biagi_N2.txt",
		"../../../../../../cross-sections/Biagi_Ar.txt" };

	// Only interested in collisions where the reactant are the neutrals in this example
	std::vector<std::string> names = { "N2", "Ar" };

	// Species prevalence of the two neutrals - choosing half and half here for example
	std::vector<double> fracs = { 0.5, 0.5 };

	// Begin with an empty library
	lib::Library lib = lib::Library();

	lib.add_xsecs_from_LXCat_files(Xsec_fids);	// add all collisions from all files
	lib.keep_only(names);						// erase all collisions which do not belong to the species we are interested in
	lib.assign_fracs_by_names(names, fracs);	// declare the fraction of the species


	/* BoltzmannParameters Definition */

	mb::BoltzmannParameters p = mb::BoltzmannParameters();

	p.model = mb::ModelCode::HD;
	// p.GE = true;
	p.N_terms = 6;		// number of terms

	p.Nu = 1000;		// grid resolution

	p.iter_max = 100;	// Force break if you haven't converged within this many iterations to avoid wasting time
	p.iter_min = 4;		// Force continue if below this number of iterations
	p.weight_f0 = 1.0;	// Iteration weight. N2 and Ar behave well, 1.0 will suffice.

	p.conv_err = 1e-6;	// relative error


	p.USE_ENERGY_REMAP = true;	// Look for a new grid if necessary
	p.remap_grid_trial_max = 10;				// try a new grid no more than this many times
	p.remap_target_order_span = 10.0;			// how many orders would you prefer your EEDF to span (in eV^-3/2)


	/* grid max start: use one or the other*/

	/*p.USE_EV_MAX_GUESS = false;
	p.USE_EV_MAX_GUESS = false;*/
	p.initial_eV_max = 100;

	// p.EN_Td = 100; // <- do not need to define this, will be overwritten in sweep
	p.p_Torr = 760; // pressure
	p.T_K = 300;	// temperature



	/* Perform calculation */


	// In this example, sweep over values of EN_Td

	std::vector<double> sweep = { 100, 200, 300, 400, 500 };


	// Set up exporter initially outside of loop
	mb::Exporter exp = mb::Exporter();

	/* Export settings and paths */
	std::string export_location = mb::UNALLOCATED_STRING;		// <- will do default location 
	std::string export_name = "example_sweep_calculations_EN_Td";	// <- name of output dir
	mb::sweep_option sweep_op = mb::sweep_option::EN_Td; 		// <- tell the exporter you are sweeping over EN_Td

	for (auto it = sweep.begin(); it != sweep.end(); it++) {
		p.EN_Td = *it; // set variable


		mb::BoltzmannSolver solver = mb::BoltzmannSolver(p, lib); // calculate
		mb::BoltzmannOutput out = solver.get_output();	// get output struct

		// on the first iteration,
		// initialize file tree structure by constructing object
		if (it == sweep.begin()) {
			exp = mb::Exporter(lib, p, out, export_location, export_name, sweep_op);
		}

		// then, do the actual writing of data
		exp.write_this_run(p, out);



	}


	return 0;
}