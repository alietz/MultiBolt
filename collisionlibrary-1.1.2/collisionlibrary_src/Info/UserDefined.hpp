// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 
#pragma once
#ifndef COLLISIONLIBRARY_USERDEFINED
#define COLLISIONLIBRARY_USERDEFINED


#include "collisionlibrary"





// For Xsecs which come from non-standard methods, are guesses, or otherwise.
//class UserDefined : public lib::AbstractInfo {
//public:
//
//	UserDefined(std::string process, std::string reactant, std::string product, std::string reference) {
//		this->_reference = reference;
//	}
//
//	void print() {
//		std::cout << "User-defined cross-section. " + _reference << std::endl;
//	}
//
//};

#endif