// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_ABSTRACTINFO
#define COLLISIONLIBRARY_ABSTRACTINFO


#include "collisionlibrary"



// An 'info' is the object held by a Xsec which is used to print its reference.
// Possessing an 'info' is not strictly required to evaluate a cross-section.
//class AbstractInfo {
//public:
//
//
//	virtual void print() = 0; 
//
//	std::string _reference;
//
//	std::string reference() { return _reference; }
//
//};

class Info {

	private:

		std::vector<std::string> _raw = { lib::UNALLOCATED_STRING };

		std::string _file = lib::UNALLOCATED_STRING;

		std::string _database = lib::UNALLOCATED_STRING;

		std::string _reference = lib::UNALLOCATED_STRING;

		std::string _date = lib::UNALLOCATED_STRING;

	public:

		Info() {};

		Info(const std::vector<std::string> raw) {

			this->_raw = raw;

		};

		std::string file() { return this->_file; };
		void file(std::string s) { this->_file = s; };

		std::string database() { return this->_database; };
		void database(std::string s) { this->_database = s; };

		std::string reference() { return this->_reference; };
		void reference(std::string s) { this->_reference = s; };

		std::string date() { return this->_date; };
		void date(std::string s) { this->_date = s; };

		void print() { std::cout << this->_reference << std::endl; };

		void print_raw() {
			std::cout << std::endl;
			for (auto& s : this->_raw) {
				std::cout << s << std::endl;
			}
		}
};


#endif





