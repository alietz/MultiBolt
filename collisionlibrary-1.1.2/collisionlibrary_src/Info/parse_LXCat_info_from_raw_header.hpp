// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 
#pragma once
#ifndef COLLISIONLIBRARY_PARSELXCAT
#define COLLISIONLIBRARY_PARSELXCAT


#include "collisionlibrary"

// first will be reactant
// second will be product
std::vector<std::string> find_rotvib_species_from_raw_header(std::vector<std::string> raw_header) {

	std::string reactant = raw_header[1].replace(0, 13, ""); // clean out "SPECIES: e / " tag

	std::string rotvib_process = raw_header[2];

	std::string delimiter;
	if (rotvib_process.find("<->", 0) != std::string::npos) { // if double arrow is  somewhere
		delimiter = "<->";
	}
	else { // if double arrow is nowhere
		delimiter = "->";
	}

	auto bodies = lib::split(rotvib_process, delimiter);

	auto tag = " E + ";
	auto pos = bodies.at(1).find(tag);
	if (pos != std::string::npos) {
		bodies.at(1) = bodies.at(1).erase(pos, std::string(tag).length());
	}
	else {
		std::string str("Attempting to locate the species in the rot/vib collision with the process string '" + rotvib_process + "' failed: could not find the reactant.");
		lib::error_statement(str.c_str());
	}


	if (rotvib_process.find("Rotational") != std::string::npos) { tag = ", Rotational"; }
	else if(rotvib_process.find("Vibrational") != std::string::npos) { tag = ", Vibrational"; }
	else {
		std::string str("Collision with process string '" + rotvib_process + "' could not be parsed as rotational or vibrational.");
		lib::error_statement(str.c_str());
	}
	

	pos = bodies.at(1).find(tag);
	if (pos != std::string::npos) {
		bodies.at(1) = bodies.at(1).erase(pos, std::string(tag).length());
	}
	else {
		std::string str("Attempting to locate the species in the rot/vib collision with the process string '" + rotvib_process + "' failed: could not find the product.");
		std::runtime_error(str.c_str());
	}

	std::string product = bodies.at(1);


	return { reactant, product };
}






// Collect all data (if applicable) that could possibly by found in the collision-centric header
// in an LXCat data file
std::tuple<lib::CollisionCode, std::string, std::string, double, double, double, std::string>
	parse_LXCat_info_from_raw_header(std::vector<std::string> raw_header) {

	/* tuple index is parsed in:
	0->code
	1->reactant
	2->product
	3->Mratio
	4->threshold
	5->g
	6->process
	*/
	//above doesn't follow for rotations! that format is just really annoying!

	double Mratio = 0;
	double g = 1.0;
	double eV_thresh = 0;
	std::string process = lib::UNALLOCATED_STRING;

	std::tuple<lib::CollisionCode, std::string, std::string, double, double, double, std::string> tup;




	auto code = lib::collision_str_code_map.find(raw_header[0])->second;

	// get the process string
		for (auto& str : raw_header) {
			if (str.find("PROCESS") != std::string::npos) {
				std::string ds = lib::trim_copy(lib::split(lib::trim_copy(str), "PROCESS: ")[1]);
				process = ds;
				break;
			}
		}


	std::string reactant, product = lib::UNALLOCATED_STRING;
	if (code == lib::CollisionCode::rotational || code == lib::CollisionCode::vibrational) {

		auto bodies = lib::find_rotvib_species_from_raw_header(raw_header);
		reactant = bodies[0];
		product = bodies[1];

		// use the PARAMS line to find eV_thresh and g_ratio.
		// in a normal excitation, would be on their own line, but oh well!

		// param line is apparently row 3
		std::string PARAM = raw_header.at(3);


		// find E
		std::string tag1 = " E = ";
		auto pos1 = PARAM.find(tag1);
		std::string tag2 = " eV,";
		auto pos2 = PARAM.find(tag2);

		eV_thresh = std::stod(PARAM.substr(pos1 + tag1.length(), (pos2 - pos1 - tag1.length())));

		// find g_ratio
		tag1 = ", g1/g0 = ";
		pos1 = PARAM.find(tag1);
		tag2 = ", complete set";
		pos2 = PARAM.find(tag2);

		g = std::stod(PARAM.substr(pos1 + tag1.length(), (pos2 - pos1 - tag1.length())));


	}
	else {
		// get reactant
		std::string line = raw_header[1];
		std::string delimiter;
		if (line.find("<->") != std::string::npos) {
			delimiter = "<->";
		}
		else {
			delimiter = "->";
		}
		reactant = lib::trim_copy(lib::split(raw_header[1], delimiter)[0]);

		// get product using same delimiter
		auto vec = lib::split(raw_header[1], delimiter);
		product = lib::trim_copy(vec[vec.size() - 1]);


		// parsing special - Mratio, thersh, g
		std::vector<std::string> ds = lib::split(lib::trim_copy(raw_header[2]), " ");

		// remove "empty entries"
		auto Nsize = ds.size();
		for (int i = 0; i < Nsize; ++i) {
			if (lib::same_string("", ds[i])) {
				ds.erase(ds.begin() + i);
				--i;
				--Nsize;
			}
		}



		if (code == lib::CollisionCode::elastic || code == lib::CollisionCode::effective) {
			Mratio = stod(ds[0]); // first should be ratio
		}
		else if (code == lib::CollisionCode::ionization) {
			eV_thresh = stod(ds[0]); // first should be thresh
		}
		else if (code == lib::CollisionCode::excitation) {
			eV_thresh = stod(ds[0]); // first should be thresh

			if (ds.size() > 1) {
				g = stod(ds[1]); // second should be g, if it exists
			}
			else {
				g = 1.0;
			}
		}
	}

	return std::tuple<lib::CollisionCode, std::string, std::string, double, double, double, std::string>
	{code, reactant, product, Mratio, eV_thresh, g, process};
}

#endif