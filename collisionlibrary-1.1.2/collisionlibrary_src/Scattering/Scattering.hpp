// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 


#pragma once

#ifndef COLLISIONLIBRARY_SCATTERING
#define COLLISIONLIBRARY_SCATTERING

// A scattering object works by knowing the function for Ibar
// What's important is evaluating the solid angle integrals including Ibar and Pell
// If the user knows an analytic (fast) evaluation for the integral, you can override the
// comparatively-slow integral user the functions called "user_"
class Scattering {

	public:
		bool integral_USE_SLOW_EVAL = false;
		bool ratio_USE_SLOW_EVAL = false;

	public:

	std::string name;

	// chi-grid settings
	bool grid_chi_USE_LIN = true;		// use linear space with chi grid, use logarithmic instead if false
	double grid_chi_log10_floor = -2;		// 10^x is lowest nonzero value in logarithmic grid
	double grid_chi_log10_minimum = -12;	// absolute minimum of log values allowable
	int grid_chi_N = lib::ANGULAR_N;




	// angular scattering treatment, accepts energy in eV and angle in radians
	// 11/18/2022 - "inelastic" also accepts the post-collision energy
	//		Some scattering functions use pre or post, or both!
	std::function<double(const double, const double, const double)> Ibar;

	// (Monte Carlo friendly) cos of RNG=[0,1] scattering angle. accepts random number R, energy in eV
	// not currently set in all conditions (todo later)
	std::function<double(const double, const double, const double)> cos_chi;


	// allows there to be a fast-eval method if one is known by the user
	std::function<double(const double, const double, const arma::sword)> user_scattering_integral;
	std::function<double(const double, const double)> user_ratio_elasticTotal_to_elasticMT;




	// default case: assume using Isotropic scattering
	Scattering() {

		this->integral_USE_SLOW_EVAL = false;
		this->ratio_USE_SLOW_EVAL = false;

		this->Ibar = [](const double eV, const double eVprime, const double chi) { return 1.0 / (4.0 * lib::PI); };

		this->user_scattering_integral = [](const double eV, const double eVprime, const arma::sword ell) {
			if (ell == 0) { return 1.0; }
			else { return 0.0; }
		};

		this->user_ratio_elasticTotal_to_elasticMT = [](const double eV, const double eVprime) { return 1.0; };


		this->cos_chi = [](const double R, const double eV, const double eVprime) {
			return 1.0 - 2.0 * R;
		};
		this->name = "Isotropic";
	};




	// if you only pass in an Ibar, assume you can only ever do slow-evals of the integral
	Scattering(std::function<double(const double, const double, const double)> foo_Ibar) {

		this->integral_USE_SLOW_EVAL = true;
		this->ratio_USE_SLOW_EVAL = true;
		this->Ibar = foo_Ibar;

		this->name = "UserDefined";

	};

	

	// user constructed passing in specific functions, you definitely want to use the fast evals as provided
	Scattering(std::function<double(const double, const double, const double)> foo_Ibar,
		std::function<double(const double, const double, const arma::sword)> foo_user_scattering_integral,
		std::function<double(const double, const double)> foo_user_elasticTotal_from_elasticMT) {

		this->integral_USE_SLOW_EVAL = false;
		this->ratio_USE_SLOW_EVAL = false;

		this->Ibar = foo_Ibar;

		this->user_scattering_integral = foo_user_scattering_integral;

		this->user_ratio_elasticTotal_to_elasticMT = foo_user_elasticTotal_from_elasticMT;
		
		// cos_chi undefined

		this->name = "UserDefined";
	}


	// fetch grid of chi angles based on mode (check member variables)
	arma::colvec get_grid_chi() {

		if (this->grid_chi_USE_LIN == true) {
			return arma::linspace(0, lib::PI, grid_chi_N);
		}
		else {
			arma::colvec angles(grid_chi_N, arma::fill::zeros);
			angles.subvec(1, grid_chi_N - 1) = arma::logspace(grid_chi_log10_floor, log10(lib::PI), grid_chi_N - 1);
			return angles;
		}
	}

	// Check if the chi_grid works for this case, find a better one if it doesn't
	// It works if it causes the scattering integral to obey the normalization condition of Ibar for ell=0
	//		11/18/2022 - because Ibar might depend on pre-or-post collision energy
	void find_better_grid_chi(const arma::colvec& eV, const arma::colvec& eVprime) {
	
		// TODO: current method is kindve naive. could do better.

		bool IS_GOOD = false; // flip when found to be good

		int grid_attempts = 0;

		//arma::colvec eV = arma::logspace(-3, 3, 20);

		bool original_USE_LIN = grid_chi_USE_LIN;
		int original_log10_floor = (int)grid_chi_log10_floor;

		grid_chi_USE_LIN = true; // always try linear first

		while (!IS_GOOD) {
		
			grid_attempts++;

			arma::colvec numeric = scattering_integral(eV, eVprime, 0); // for ell =0

			numeric.replace(arma::datum::nan, 0);

			arma::uvec is_not_zeros = arma::find(numeric != 0);
			// zeros/nans may appear at eV for scattering that actually depends on threshold energies
			// and those are fine, you can ignore them


			arma::uvec meets_condition = (arma::abs(numeric(is_not_zeros) - 1) < 1e-2);


			if (arma::all(meets_condition)) {
				IS_GOOD = true;
				//numeric.print();
				break;
			}
			else {

				this->grid_chi_USE_LIN = false;

				this->grid_chi_log10_floor -= 1; // -1, -2, -3, etc

				this->grid_chi_log10_floor = std::max(this->grid_chi_log10_floor, this->grid_chi_log10_minimum);

				if (this->grid_chi_log10_floor <= this->grid_chi_log10_minimum) {
					lib::normal_statement("Warning: Scattering '" + this->name + "' had a difficult time finding a new grid for chi. Returning to original state.");
					this->grid_chi_USE_LIN = original_USE_LIN;
					this->grid_chi_log10_floor = original_log10_floor;


					break;
				}
			}
		}
	}



	// Scattering-angle-"two", evaluate for inelastic collisions
	double scattering_integral(const double eV, const double eVprime, const arma::sword ell) {
		
		if ( this->integral_USE_SLOW_EVAL == true) {
		
			if (ell == 0) {
				return 1.0; // should happen always by-definition
			}
				
			arma::colvec angles = this->get_grid_chi();
			arma::colvec legendreP_cosx(grid_chi_N, arma::fill::zeros);
			arma::colvec Ibar_eval(grid_chi_N, arma::fill::zeros);

			for (int i = 0; i < angles.n_elem; ++i) {
				legendreP_cosx(i) = std::legendre(int(ell), cos(angles.at(i)));
				Ibar_eval(i) = Ibar(eV, eVprime, angles.at(i));
			}

			double eval = 2.0 * lib::PI * arma::accu(arma::trapz(angles, legendreP_cosx % sin(angles) % Ibar_eval));
			return eval;

		}
		else {
			return user_scattering_integral(eV, eVprime, ell);
		}
	}

	// overload for vector pass-in
	arma::colvec scattering_integral(const arma::colvec eV, const arma::colvec eVprime, const arma::sword ell) {

		arma::colvec evals(eV.n_elem, arma::fill::zeros);
		for (int i = 0; i < eV.n_elem; ++i) {
			evals.at(i) = scattering_integral(eV.at(i), eVprime.at(i), ell);
		}
		return evals;
	}



	

	
	// Ibar is used to infer the shape of the total elastic cross section from the momentum transfer xsec which
	// is otherwise conserved
	double ratio_elasticTotal_to_elasticMT(const double eV, const double eVprime) {

		if (ratio_USE_SLOW_EVAL == true) {
			
			arma::colvec angles = this->get_grid_chi();
			arma::colvec Ibar_eval(grid_chi_N, arma::fill::zeros);

			for (int i = 0; i < angles.n_elem; ++i) {
				Ibar_eval(i) = Ibar(eV, eVprime, angles.at(i));
			}
			
			double RATIO = 2.0 * lib::PI * arma::accu(arma::trapz(angles, (1.0 - cos(angles)) % sin(angles) % Ibar_eval));
			return  1.0 / RATIO;

		}
		else {
			return user_ratio_elasticTotal_to_elasticMT(eV, eVprime);
		}
	}

	// 11/23/2022 - use intergration to check normalization of Ibar at the given eV
	bool Ibar_obeys_normalization(double eV, double eVprime) {

		double numeric = scattering_integral(eV, eVprime, 0);

		if (abs(abs(numeric - 1.0) / 1.0) < (1.0 / 100)) {
			return true;
		}
		else {
			return false;
		}
	}


};


// ------------------------------------------------------
// several scattering objects which handle the common cases

// Most common assumption: electrons do not prefer any particular angle
lib::Scattering isotropic_scattering() {

	lib::Scattering X = lib::Scattering();

	X.name = "Isotropic";

	return X;
}

// All electrons skip forward with unchanging collision angle
// Per the following: 
// J. Janssen, L. Pitchford, G. Hagelaar, and J. van Dijk,
// “Evaluation of angular scattering models for electron-neutral collisions in Monte Carlo simulations,” Plasma Sources Science and Technology, vol. 25, Art. no. 5, 2016.
// Uses Ibar = 1/2/pi * dirac(chi) / sin(chi)
lib::Scattering forward_scattering() {

	// todo: remove when validation complete
	lib::normal_statement("Developer's note: *** WARNING *** Forward scattering is not yet validated. *** USE AT YOUR OWN RISK ***.");

	std::function<double(const double, const double, const double)> foo_Ibar = 
		[](const double eV, const double eVprime, const double chi) {
		if (chi == 0) { return (1.0 / (4.0 * lib::PI)); }
		else { return 0.0; }
	};

	std::function<double(const double, const double, const arma::sword)> foo_user_scattering_integral = 
		[](const double eV, const double eVprime, const arma::sword ell) {
		return 1.0;
	};

	std::function<double(const double, const double)> foo_user_elasticTotal_to_elasticMT = 
		[](const double eV, const double eVprime) {
		return arma::datum::nan;
	};

	lib::Scattering X
		= lib::Scattering(foo_Ibar, foo_user_scattering_integral, foo_user_elasticTotal_to_elasticMT);

	X.cos_chi = [](const double R, const double eV, const double eVprime) {
		return 1.0;
	};

	X.name = "IdealForward";

	return X;

}



// Energy-dependent scattering which is more forward for higher energy collisions and more isotropic for lower energies
// A. Okhrimovskyy, A. Bogaerts, and R. Gijbels,
// “Electron anisotropic scattering in gases: A formula for Monte Carlo simulations,” Physical Review E, vol. 65, Art. no. 3, 2002.
lib::Scattering screened_coulomb_scattering(double SCREEN_EV = lib::HARTREE) {

	// todo: remove when validation complete
	lib::normal_statement("Developer's note: *** WARNING *** Screened Coulomb scattering is not yet validated. *** USE AT YOUR OWN RISK ***.");

	std::function<double(const double, const double, const double)> foo_Ibar = 
		[SCREEN_EV](const double eV, const double eVprime, const double chi) {

		double XI = 4 * eVprime / SCREEN_EV / (1 + 4 * eVprime / SCREEN_EV); // specifically uses post-collision energy
		return 1.0 / (4 * lib::PI) * (1 - XI * XI) / pow((1 - XI * cos(chi)), 2);
	};

	std::function<double(const double, const double)> foo_ratio = // ratio of cross sections just needs incident electron energy
		[SCREEN_EV](const double eV, const double eVprime) {

		double XI = 4 * (eV / SCREEN_EV) / (1 + 4 * (eV / SCREEN_EV));
		double ratio = (1 - XI) / (2 * XI * XI) * ((1 + XI) * log((1 + XI) / (1 - XI)) - 2 * XI);
		return 1.0 / ratio;
	};

	lib::Scattering X = lib::Scattering(foo_Ibar);
	X.name = "ScreenedCoulomb(" + std::to_string(SCREEN_EV) + " eV)";

	X.user_ratio_elasticTotal_to_elasticMT = foo_ratio;
	X.ratio_USE_SLOW_EVAL = false;

	X.cos_chi = [SCREEN_EV](const double R, const double eV, const double eVprime) {

		double eta = SCREEN_EV / (8 * eVprime); // using post-collision energy
		double cos_chi = (eta + 1 - R * (1 + 2 * eta)) / (eta + 1 - R);
		return cos_chi;
	};

	return X;
}

// A placeholder for invalid scattering (ie, electron attachment, where there is no scattered electron)
lib::Scattering no_scattering() {


	std::function<double(const double, const double, const double)> foo_Ibar = [](const double eV, const double eVprime, const double chi) {
		return arma::datum::nan;
	};

	std::function<double(const double, const double, const arma::sword)> foo_user_scattering_integral = [](const double eV, const double eVprime, const arma::sword ell) {
		return arma::datum::nan;
	};

	std::function<double(const double, const double)> foo_user_elasticTotal_to_elasticMT = [](const double eV, const double eVprime) {
		return arma::datum::nan;
	};

	lib::Scattering X
		= lib::Scattering(foo_Ibar, foo_user_scattering_integral, foo_user_elasticTotal_to_elasticMT);

	X.name = "Not-Applicable";

	return X;

}



// Scattering fit for rotational collisions of polar linear moelcules (ex: CO, which has a strong dipole)
// based on Born approximation for point-dipole interaction.
// Implemented per Vialetto et al 2021, DOI: 10.1088/1361-6595/ac0a4d
// See also: Crawford 1967, DOI: 10.1063/1.1711993
lib::Scattering dipole_BornApprox_scattering() {

	std::function<double(const double, const double, const double)> foo_Ibar = [](const double eV, const double eVprime, const double chi) {

		if (eVprime < 0) { return 0.0; }

		double K_squared = (eV + eVprime) - 2.0 * sqrt(eV * eVprime) * cos(chi);

		// Equivalent to as it appears in Vialetto et al 2021, DOI: 10.1088/1361-6595/ac0a4d
		double Ibar = 1.0 / lib::PI / 4.0 * sqrt(eV) * sqrt(eVprime) / K_squared * (1.0 / (log((sqrt(eVprime) + sqrt(eV)) / sqrt(abs(eVprime - eV)))));

		return Ibar;
	};

	lib::Scattering X = lib::Scattering(foo_Ibar);
	X.name = "Dipole_BornApprox";

	//X.user_ratio_elasticTotal_to_elasticMT = foo_ratio;
	X.ratio_USE_SLOW_EVAL = true;

	X.integral_USE_SLOW_EVAL = true;

	X.cos_chi = [](const double R, const double eV, const double eVprime) {

		// Implemented per Vialetto et al 2021, DOI: 10.1088/1361-6595/ac0a4d
		double eV_thresh = abs(eVprime - eV);
		double XSI = eV_thresh / ((sqrt(eVprime) + sqrt(eV)) * (sqrt(eVprime) + sqrt(eV)));

		double cos_chi = 1 + 2 * XSI * XSI / (1 - XSI * XSI) * (1 - pow(XSI, -2 * R));

		return cos_chi;
	};

	// you'd rather use a log-based grid
	X.grid_chi_USE_LIN = false;		
	X.grid_chi_log10_floor = -7;		


	return X;

}

// 4/3/2023
// All electrons bounce backwards, ie, opposite of forward scattering
// Uses Ibar = 1/2/pi * dirac(chi - pi) / sin(chi)
lib::Scattering backward_scattering() {

	// todo: remove when validation complete
	lib::normal_statement("Developer's note: *** WARNING *** Backward scattering is not yet validated. *** USE AT YOUR OWN RISK ***.");

	std::function<double(const double, const double, const double)> foo_Ibar =
		[](const double eV, const double eVprime, const double chi) {
		if (chi == lib::PI) { return (1.0 / (4.0 * lib::PI)); }
		else { return 0.0; }
	};

	std::function<double(const double, const double, const arma::sword)> foo_user_scattering_integral =
		[](const double eV, const double eVprime, const arma::sword ell) {
		if (ell % 2 == 0) {
			return 1.0;
		}
		else {
			return -1.0;
		}
	};

	std::function<double(const double, const double)> foo_user_elasticTotal_to_elasticMT =
		[](const double eV, const double eVprime) {
		return 1.0 / 2.0;
	};

	lib::Scattering X
		= lib::Scattering(foo_Ibar, foo_user_scattering_integral, foo_user_elasticTotal_to_elasticMT);

	X.cos_chi = [](const double R, const double eV, const double eVprime) {
		return -1.0;
	};

	X.name = "IdealBackward";

	return X;

}

#endif