// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_FUNCTIONALXSEC
#define COLLISIONLIBRARY_FUNCTIONALXSEC
#include "collisionlibrary"

// For analyitcally defined Xsecs
// In this form, define a std::function foo
// Expects one double in, and one double out.
class FunctionalXsec : virtual public lib::AbstractXsec {

public:
	std::function<double(double)> foo;

	FunctionalXsec() { this->scattering(lib::isotropic_scattering()); };
	FunctionalXsec(std::function<double(double)>& foo, lib::CollisionCode code) {
		this->code(code);
		this->foo = foo;
		this->scattering(lib::isotropic_scattering());
	}

	void print() { 
		std::cout << _process << std::endl;

		switch (this->code()) {

		case lib::CollisionCode::elastic: std::cout << "mratio: " << this->Mratio() << std::endl; break;
		case lib::CollisionCode::effective: std::cout << "mratio: " << this->Mratio() << std::endl; break;
		case lib::CollisionCode::excitation: std::cout << "E: " << this->eV_thresh() << std::endl; break;
		case lib::CollisionCode::superelastic: std::cout << "parent_E: " << this->parent_eV_thresh() << "\t g: " << this->g() << std::endl; break;
		case lib::CollisionCode::ionization: std::cout << "E: " << this->eV_thresh() << std::endl; break;
		}
	}

	
	double eval_at_eV(const double eV, const lib::InterpolationMethod interp_method = lib::DEFAULT_INTERP_METHOD) {
		return foo(eV) * _scale; // 12/2022 - should really be appling the scale to the inner most layer
	}

	arma::colvec eval_at_eV(const arma::colvec& eV, const lib::InterpolationMethod interp_method = lib::DEFAULT_INTERP_METHOD) {
		arma::colvec temp = arma::colvec(eV.n_elem, arma::fill::zeros);

		arma::sword k = 0;
		for (double e : eV) {
			temp(k) = foo(e) * _scale;
			++k;
		}
		return temp;
	}

};

#endif

