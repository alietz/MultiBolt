// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_ABSTRACTXSEC
#define COLLISIONLIBRARY_ABSTRACTXSEC
#include "collisionlibrary"


class AbstractXsec {


public:

	// 04/28/2023 -  using gridded_s is still allowable, but deprecated now
	// because it happens to mesh poorly with parallelization in multibolt

	std::map<arma::sword, arma::colvec> gridded_s;
	// as many grids as are needed are allowed, but the user is responsible
	// for keeping track of what means what
	
	// in the case of multibolt, this means that multibolt has two codes
	// EVEN = 0
	// and
	// ODD = 1
	// and gridded_s[EVEN] is the even-grid,
	// and gridded_s[ODD] is the odd-grid
	


protected:

	lib::Info _info;

	lib::CollisionCode _code = lib::CollisionCode::nocollision;

	double _scale = 1.0;

	double _Mratio = 0; // only supposed to be valid for elastic, and effective
	double _g = 1.0; // only useful for superelastic
	double _eV_thresh = 0; // only useful for excitations and ionizations

	double _parent_eV_thresh = 0; // only useful for superelastic



	std::string _product;
	std::string _reactant;
	std::string _process; // consider this to be a unique identifier. 

	std::shared_ptr<lib::Scattering> _scattering;

// -------------------	
// functions (setters)
public:
	
	void Mratio(double x) {
		if (_code == lib::CollisionCode::elastic || _code == lib::CollisionCode::effective) {
			_Mratio = x;
		}
		else {
			lib::bad_Mratio_interaction();
		}
	}

	void g(double x) {
		if (_code == lib::CollisionCode::superelastic) {
			_g = x;
		}
		else {
			lib::bad_g_interaction();
		}
	}

	void eV_thresh(double x) {
		if (_code == lib::CollisionCode::excitation || _code == lib::CollisionCode::ionization) {
			_eV_thresh = x;
		}
		else {
			lib::bad_eV_thresh_interaction();
		}
	}

	void parent_exc_thresh(double x) {
		if (_code == lib::CollisionCode::superelastic) {
			_parent_eV_thresh = x;
		}
		else {
			lib::bad_parent_eV_thresh_interaction();
		}
	}

	void process(std::string str) { _process = str; }
	void product(std::string str) { _product = str; }
	void reactant(std::string str) { _reactant = str; }

	void code(lib::CollisionCode c) { _code = c; }

	std::string process () const { return _process; }
	std::string product () const { return _product; }
	std::string reactant () const { return _reactant; }

	
	lib::Info info() const {
		return _info;
	}
	void info(lib::Info i) {
		this->_info = i;
	}

	void scattering(std::shared_ptr<lib::Scattering> s) {
		this->_scattering = s;
	}
	void scattering(lib::Scattering s) {
		this->_scattering = std::make_shared<lib::Scattering>(s);
	}
	


// -------------------	
// functions (getters)
	double Mratio () const {
		if (_code == lib::CollisionCode::elastic || _code == lib::CollisionCode::effective) {
			return _Mratio;
		}
		else {
			lib::bad_Mratio_interaction();
			return arma::datum::nan;
		}
	}

	double g () const {
		if (_code == lib::CollisionCode::superelastic) {
			return _g;
		}
		else {
			lib::bad_g_interaction();
			return arma::datum::nan;
		}
	}

	double eV_thresh () const {
		if (_code == lib::CollisionCode::excitation || _code == lib::CollisionCode::ionization) {
			return _eV_thresh;
		}
		else {
			lib::bad_eV_thresh_interaction();
			return arma::datum::nan;
		}
	}

	double parent_eV_thresh() const {
		if (_code == lib::CollisionCode::superelastic) {
			return _parent_eV_thresh;
		}
		else {
			lib::bad_parent_eV_thresh_interaction();
			return arma::datum::nan;
		}
	}

	std::shared_ptr<lib::Scattering> scattering() const {
		return this->_scattering;
	}







	virtual void print() = 0;

	// deprecated
	//virtual void set_grid(const arma::sword gkey, const arma::colvec& eV, const lib::InterpolationMethod interp_method = lib::DEFAULT_INTERP_METHOD) = 0;


	virtual double eval_at_eV(const double eV, const lib::InterpolationMethod interp_method = lib::DEFAULT_INTERP_METHOD) = 0;
	virtual arma::colvec eval_at_eV(const arma::colvec& eV, const lib::InterpolationMethod interp_method = lib::DEFAULT_INTERP_METHOD) = 0;

	// use armadillo errors for the following in edge-cases
	double eval_at_k(const arma::sword gkey, const arma::sword k) {
		return gridded_s[gkey](k);
	};

	arma::colvec eval_at_k(const arma::sword gkey, const arma::uvec& k) {
		return gridded_s[gkey](k);
	};

	double scale() const { return _scale; }
	void scale(double s) { _scale = s; }

	lib::CollisionCode code() const { return _code; }
	
	
	void set_grid(const arma::sword gkey, const arma::colvec& eV, const lib::InterpolationMethod interp_method = lib::DEFAULT_INTERP_METHOD) {
		arma::colvec temp = eV * 0; // correct size

		gridded_s[gkey] = eval_at_eV(eV, interp_method);

		lib::debug_statement("Xsec with process [" + process() + "] gridded with gkey [" + std::to_string(gkey) + "].");
		return;
	}

};

#endif



