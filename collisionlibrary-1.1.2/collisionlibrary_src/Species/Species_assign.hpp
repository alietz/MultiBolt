// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_SPECIESASSIGN
#define COLLISIONLIBRARY_SPECIESASSIGN
#include "collisionlibrary"

void lib::Species::assign_scale_by_process(std::string process, double s) {
	for (auto it = allcollisions.begin(); it != allcollisions.end(); ++it) {
		if ((*it)->process().compare(process) == 0) {


			lib::debug_statement("Assigned scale [" + std::to_string(s) + "] to Xsec with process [" + process + "]");
			(*it)->scale(s);

		}
	}
}

// to assign nth collision regardless of type
void lib::Species::assign_scale_by_index(int k, double s) {

	if (k > n_all()) {
		lib::bad_index(k);
		return;
	}

	std::string temp_process = allcollisions.at(k)->process();
	assign_scale_by_process(temp_process, s);
}

// case for when the collision-type is known
void lib::Species::assign_scale_by_index(lib::CollisionCode c, int k, double s) {

	XsecPtrVec* ptr = pick_from_code(c);

	if (k > ptr->size()) {
		lib::bad_index(k);
		return;
	}

	std::string temp_process = (*ptr).at(k)->process();
	assign_scale_by_process(temp_process, s);
}



// for only this species, assign all collisions of one type to this scattering
void lib::Species::assign_scattering_by_type(lib::CollisionCode c, lib::Scattering scattering) {

	lib::debug_statement("Assigning scattering-type [" + scattering.name + "] to all collisions of type [" + lib::collision_code_str_map.find(c)->second + 
		"] in the species [" + this->_name + "].");


	auto ptr = pick_from_code(c);

	for (auto x : *ptr) {
		x->scattering(scattering);
	}
}

// assign scattering to the collision with index k in allcollisions
void lib::Species::assign_scattering_by_index(int k, lib::Scattering scattering) {

	if (k > allcollisions.size()) {
		lib::bad_index(k);
		return;
	}
	lib::debug_statement("Assigning scattering-type [" + scattering.name + "] to collision at index [" + std::to_string(k) +
		"] in the species [" + this->_name + "].");
	allcollisions[k]->scattering(scattering);

}

void lib::Species::assign_scattering_by_index(lib::CollisionCode c, int k, lib::Scattering scattering) {
	

	auto ptr = pick_from_code(c);

	if (k > ptr->size()) {
		lib::bad_index(k);
		return;
	}

	lib::debug_statement("Assigning scattering-type [" + scattering.name + "] to " + lib::collision_code_str_map.find(c)->second +
		" collision at index [" + std::to_string(k) + "] in the species [" + this->_name + "].");

	ptr->at(k)->scattering(scattering);

	
}

void lib::Species::assign_scattering_by_process(std::string process, lib::Scattering scattering) {

	for (auto it = allcollisions.begin(); it != allcollisions.end(); ++it) {
		if ((*it)->process().compare(process) == 0) {


			lib::debug_statement("Assigned scattering-type [" + scattering.name + "] to Xsec with process [" + process + "]");
			(*it)->scattering(scattering);

		}
	}
}




#endif