// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#ifndef COLLISIONLIBRARY_REIDRAMP
#define COLLISIONLIBRARY_REIDRAMP

#include "collisionlibrary"


// See: Reid I D 1979 Aust J. Phys. 32 231
lib::Library get_ReidRamp_Library(double ELA_HEIGHT = 6e-20, double MASS = 4, double EV_THRESH = 0.2, double EXC_SLOPE = 1e-19) {

	lib::Library lib = lib::Library();
	lib.add_blank_species("ReidRamp_neutral");
	lib.add_blank_species("ReidRamp_excitedstate");

	// Set up Reid Ramp elastic Xsec
	std::function<double(double)> foo1 = [ELA_HEIGHT](double eV) { return (ELA_HEIGHT); };
	lib::FunctionalXsec ReidRamp_elastic = lib::FunctionalXsec(foo1, lib::CollisionCode::elastic);
	ReidRamp_elastic.Mratio(lib::ME / (MASS * lib::AMU));
	ReidRamp_elastic.reactant("ReidRamp_neutral");
	ReidRamp_elastic.product("ReidRamp_neutral");
	ReidRamp_elastic.process("E + ReidRamp_neutral -> E + ReidRamp_neutral, Elastic");
	ReidRamp_elastic.scattering(lib::isotropic_scattering());


	// Set up Reid Ramp excitation Xsec
	std::function<double(double)> foo2 = [EV_THRESH, EXC_SLOPE](double eV) {
		if (eV < EV_THRESH) {
			return 0.0;
		}
		return (eV - EV_THRESH) * EXC_SLOPE;
	};


	lib::FunctionalXsec ReidRamp_excitation = lib::FunctionalXsec(foo2, lib::CollisionCode::excitation);
	ReidRamp_excitation.eV_thresh(EV_THRESH);
	ReidRamp_excitation.reactant("ReidRamp_neutral");
	ReidRamp_excitation.product("ReidRamp_excitedstate");
	ReidRamp_excitation.process("E + ReidRamp_neutral -> E + ReidRamp_excitedstate, Excitation");
	ReidRamp_excitation.scattering(lib::isotropic_scattering());

	lib.add_Xsec(std::make_shared<lib::FunctionalXsec>(ReidRamp_elastic));
	lib.add_Xsec(std::make_shared<lib::FunctionalXsec>(ReidRamp_excitation));

	lib.assign_fracs_by_names("ReidRamp_neutral", 1.0);

	return lib;
}

#endif