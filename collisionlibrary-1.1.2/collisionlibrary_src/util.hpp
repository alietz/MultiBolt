// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_UTIL
#define COLLISIONLIBRARY_UTIL
#include "collisionlibrary"

// various helper functions

// trim from start (in place)
inline void ltrim(std::string& s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
		return !std::isspace(ch);
		}));
}

// trim from end (in place)
inline void rtrim(std::string& s) {
	s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
		return !std::isspace(ch);
		}).base(), s.end());
}

// trim from both ends (in place)
inline void trim(std::string& s) {
	lib::ltrim(s);
	lib::rtrim(s);
}

// trim from start (copying)
inline std::string ltrim_copy(std::string s) {
	lib::ltrim(s);
	return s;
}

// trim from end (copying)
inline std::string rtrim_copy(std::string s) {
	lib::rtrim(s);
	return s;
}

// trim from both ends (copying)
inline std::string trim_copy(std::string s) {
	lib::trim(s);
	return s;
}

// helper function, cut down on some clutter on string comparison
bool same_string(std::string A, std::string B) {

	if (A.compare(B) == 0) {
		return true;
	}
	else {
		return false;
	}
}

// split string on token (default, whitespace)
std::vector<std::string> split(std::string str, std::string token) {
	std::vector<std::string> result;
	while (str.size()) {
		auto index = str.find(token);
		if (index != std::string::npos) {
			result.push_back(str.substr(0, index));
			str = str.substr(index + token.size());
			if (str.size() == 0)result.push_back(str);
		}
		else {
			result.push_back(str);
			str = "";
		}
	}
	return result;
}

// space-saver
bool is_collision_string(const std::string& line) {
	if (lib::same_string(line, lib::ATTACHMENT_STRING) ||
		lib::same_string(line, lib::ROTATIONAL_STRING) ||
		lib::same_string(line, lib::VIBRATIONAL_STRING) ||
		lib::same_string(line, lib::ELASTIC_STRING) ||
		lib::same_string(line, lib::EFFECTIVE_STRING) ||
		lib::same_string(line, lib::EXCITATION_STRING) ||
		lib::same_string(line, lib::IONIZATION_STRING)) {

		return true;
	}

	return false;
}

// Boltzmann population of vibrational levels of a simple harmonic oscillator molecule
// N_V: number of vibrational states, starting from 0. Model from V=0 to V=N_V-1
// T_g : gas temperature in Kelvin
// k: Wavenumber of harmonic frequency in [cm^-1]
arma::colvec vibrational_BoltzmannPopulation(int N_V, double T_g, double k) {

	arma::colvec BoltzPop(N_V, arma::fill::zeros);

	if (T_g <= 0) { // if cold-case, only J=0 should exist
		BoltzPop.zeros();
		BoltzPop(0) = 1.0;
		return BoltzPop;
	}

	arma::colvec Vvec = arma::regspace(0, 1, (N_V - 1));
	arma::colvec e_vib_state = lib::PLANCK_EV * k * (Vvec + 0.5);	// harmonic oscillator energy in eV

	BoltzPop = (1.0) * arma::exp(-e_vib_state / (lib::KB * T_g / lib::QE)); // Boltzmann population of state
	BoltzPop = BoltzPop / arma::sum(BoltzPop);

	return BoltzPop;
}

// Boltzmann population of rotational levels of a simple rigid rotor molecule
// N_J: number of rotational states, starting from 0. Model from J=0 to J=N_V-1
// T_g : gas temperature in Kelvin
// B: Rotational constant of molecule in [cm^-1]
arma::colvec rotational_BoltzmannPopulation(int N_J, double T_g, double B) {

	arma::colvec BoltzPop(N_J, arma::fill::zeros);

	if (T_g <= 0) { // if cold-case, only J=0 should exist
		BoltzPop.zeros();
		BoltzPop(0) = 1.0;
		return BoltzPop;
	}

	arma::colvec Jvec = arma::regspace(0, 1, (N_J - 1));
	arma::colvec e_state = Jvec % (Jvec + 1.0) * B;	// rigid rotor energy in eV
	arma::colvec g = 2.0 * Jvec + 1.0;					// degeneracy of rotational states

	BoltzPop = g % arma::exp(-e_state / (lib::KB * T_g / lib::QE)); // Boltzmann population of state
	BoltzPop = BoltzPop / arma::sum(BoltzPop);

	return BoltzPop;
}




#endif