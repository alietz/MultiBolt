// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_ERROR
#define COLLISIONLIBRARY_ERROR
#include "collisionlibrary"

// Whether or not print should be allowed to happen
bool print_err_is_allowed() {
	if (lib::libSpeaker.get_outOp() != lib::OutputOption::SILENT) {
		return true;
	}
	return false;
}



void file_not_opened_err(const std::string& filename) {

	std::string str = "The file [" + filename + "] could not be opened.";

	if (lib::print_err_is_allowed()) {
		lib::error_statement(str);
	}

	throw std::runtime_error(str);
}

void species_not_found(const std::string& name) {
	std::string str = "The species [" + name + "] could not be found.";

	if (lib::print_err_is_allowed()) {
		lib::error_statement(str);
	}

	throw std::runtime_error(str);
}

void collision_not_found(const std::string& process) {
	std::string str = "The collision with process [" + process + "] could not be found.";

	if (lib::print_err_is_allowed()) {
		lib::error_statement(str);
	}

	throw std::runtime_error(str);
}


void bad_index(int k) {
	std::string str = "Procedure failed at index [" + std::to_string(k) + "] because element did not exist.";

	if (lib::print_err_is_allowed()) {
		lib::error_statement(str);
	}

	throw std::runtime_error(str);
}

void bad_collision_code() {
	std::string str = "Development error: bad collision code in use.";

	if (lib::print_err_is_allowed()) {
		lib::error_statement(str);
	}

	throw std::runtime_error(str);
}

void bad_Mratio_interaction() {

	std::string str = "Xsecs other than 'Elastic' or 'Effective' do not have a 'Mratio': interaction disallowed.";

	if (lib::print_err_is_allowed()) {
		lib::error_statement(str);
	}

	throw std::runtime_error(str);
}

void bad_g_interaction() {

	std::string str = "Xsecs other than 'Superelastic' do not have a 'g': interaction disallowed.";

	if (lib::print_err_is_allowed()) {
		lib::error_statement(str);
	}

	throw std::runtime_error(str);
}

void bad_eV_thresh_interaction() {

	std::string str = "Xsecs other than 'Excitation' and 'Ionization' do not have a 'eV_thresh': interaction disallowed.";

	if (lib::print_err_is_allowed()) {
		lib::error_statement(str);
	}

	throw std::runtime_error(str);
}

void bad_parent_eV_thresh_interaction() {
	std::string str = "Xsecs other than 'Superelastic' do not have a 'parent_eV_thresh': interaction disallowed.";

	if (lib::print_err_is_allowed()) {
		lib::error_statement(str);
	}

	throw std::runtime_error(str);
}

void unequal_pairs_err() {
	std::string str = "Vectors passed as pairs were not equal sizes.";

	if (lib::print_err_is_allowed()) {
		lib::error_statement(str);
	}

	throw std::runtime_error(str);
}

#endif