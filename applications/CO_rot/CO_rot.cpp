// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2022 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 


// Construct a library of CO gas which includes:
// * vibrational excitations and superelastic collisions (@Boltzmann populations)
// * rotational excitations and superelastic collisions (@Boltzmann populations)
// * Allows both isotropic and analytic anisotropic scattering for rot and sup
//
// At low E/N, calculations of CO conditions require rotations and vibrations + superelastics
// to be included in order to behave well, and give meaningful answers
//
// At low E/N, anisotropic scattering for rotations mostly increases the value of muN_FLUX
// which allows slightly better fitting to experiment
//
// This script is a great example about how to do all these complicated tasks *without* 
// relying on large numbers of tabulated cross section files
// (though the Lisbon set does use tabulations for J levels due to near-threshold adjustments)

// constants relevant to vibration levels
static const double CO_K = 4.0871e+14; // Wavenumber of CO vibration, [cm^-1]


// constants relevant to dipole and quadrupole rotations
static const double CO_MRATIO = 1.950000e-5;	// Ratio between mass of electron and ground-state CO
static const double CO_MU = 4.32E-2;				// Permanent dipole moment of CO [e * a0]
static const double CO_Q = 1.86;					// Permanent quadrupole moment of CO [e * a0^2]
static const double CO_B = 2.4e-4;				// Rotational constant for CO [eV]


#include <multibolt>


int main() {

	double T_g = 300;	// Gas temperature, Kelvin

	bool USE_LISBON = false; // Use the IST-Lisbon set. Otherwise, use the Biagi-derived set (also under Lisbon database as 'CO_anis')

	// scattering to use for the rotational excitation and de-excitation
	// ISO :: isotropic
	// FWD :: forward
	// DB :: Dipole-Born (analytic)
	enum SCATTERING { ISO, FWD, DB };

	SCATTERING rotational_scattering = DB;


	// ----------------------
	// the following change depending on xsec set in use

	bool USE_ANALYTIC_ROT; // define rotations using equation (otherwise, use IST-Lisbon set)

	// not interesting enough, can ignore
	//bool INCLUDE_QUADRUPOLE = false; // include delta-J=+/- 2 rotations, isotropic scattering only


	int N_rot_dipole; // Solve inclusive of up to N_rot_dipole rotational levels (0-17, tabulated or analytic)

	int N_vib; // Solve inclusive of up to N_vib vibrational levels (0-10, tabulated)
	
	// ------------------------------
	
	if (USE_LISBON) {
		USE_ANALYTIC_ROT = false; // use tabulated form, first five J levels are reduced near the threshold

		N_vib = 10;
		N_rot_dipole = 17;

	}
	else { // use set derived from Biagi

		USE_ANALYTIC_ROT = true; // Biagi used the exact analytic form for rotations, no transformation necessary

		N_vib = 6;
		N_rot_dipole = 26;

	}

	// -- v=> 0->v+ is significant for the low E/N at 300 k, need to account for vibrational superelastics
	arma::colvec vib_BoltzPop = lib::vibrational_BoltzmannPopulation(N_vib, T_g, CO_K);

	// Using traits of CO, solve boltzmann populations for rot
	arma::colvec rot_BoltzPop = lib::rotational_BoltzmannPopulation(N_rot_dipole, T_g, CO_B);

	


	// ----------------------------------------------------------------

	// stdout mode
	mb::mbSpeaker.printmode_normal_statements();


	// BoltzmannParameters Definition //

	mb::BoltzmannParameters p = mb::BoltzmannParameters();

	p.model = mb::ModelCode::HD;
	p.N_terms = 2;

	p.Nu = 1000;

	p.iter_max = 100;	// Force break if you haven't converged within this many iterations to avoid wasting time
	p.iter_min = 4;		// Force continue if below this number of iterations
	p.weight_f0 = 1.0;	// Iteration weight.

	p.conv_err = 1e-6;	// relative error


	p.USE_ENERGY_REMAP = true;	// Look for a new grid if necessary
	p.remap_grid_trial_max = 10;				// try a new grid no more than this many times
	p.remap_target_order_span = 15.0;			// how many orders would you prefer your EEDF to span (in eV^-3/2)

	p.USE_EV_MAX_GUESS = true;
	p.DONT_ENFORCE_SUM = true; // needed for CO modeling perspective here


	p.initial_eV_max = 40;

	int N_EN = 25;
	arma::colvec sweep = arma::logspace(-2, +2, N_EN);


	arma::colvec rough_EN = { 1e-2, 1e-1, 1e0, 1e+1, 1e+2 };
	arma::colvec rough_eVmax = {1.5, 5, 10, 40, 40 };



	arma::colvec vec_eVmax;
	arma::interp1(rough_EN, rough_eVmax, sweep, vec_eVmax);


	p.EN_Td = sweep(0);
	p.p_Torr = 760;
	p.T_K = T_g;

	

	// --------------------------------------------------------------------
	std::string CO_name;
	lib::Library CO_lib = lib::Library();
	std::vector<std::string> bodies_to_keep;

	if (USE_LISBON) {
		// Receive non-rotation cross sections from IST-Lisbon database
		CO_lib.add_xsecs_from_LXCat_files("../../applications/CO_rot/CO_rot_cross-sections/ISTLisbon/ISTLisbon_CO.txt");

		// vib are between ground and v-state. v-state energy is high enough that populations are negligible.
		CO_lib.add_xsecs_from_LXCat_files("../../applications/CO_rot/CO_rot_cross-sections/ISTLisbon/ISTLisbon_CO-vib.txt");

		CO_name = "CO";
		
	}
	else{
		// Receive non-rotation cross sections from database
		CO_lib.add_xsecs_from_LXCat_files("../../applications/CO_rot/CO_rot_cross-sections/MAGBOLTZ_v11-10/CO_anis.txt");

		// retrive vib levels, seperated
		CO_lib.add_xsecs_from_LXCat_files("../../applications/CO_rot/CO_rot_cross-sections/MAGBOLTZ_v11-10/CO_anis-vib.txt");

		CO_name = "CO_anis"; // keep these names, throw other species away
	}

	int N_non_rot_species = CO_lib.n_all();
	int N_non_rot_collisions = CO_lib.allspecies[0]->n_all();

	bodies_to_keep = { CO_name }; // keep these names, throw other species away



	// the last ten collisions read in this way are the vibrationally excited states (v=1,v=2, etc)
	// they should start out ~1e-5 for v=1. v=0 should be ~1
	for (int V = 1; V < N_vib; ++V) {

		int state = CO_lib.n_all() - N_vib + V;

		CO_lib.assign_frac_by_index(state, vib_BoltzPop.at(V));
		bodies_to_keep.push_back(CO_lib.allspecies[state]->_name);
	}

	// the last N_vib excitations belonging to CO should be scaled  by the BoltzPop belonging to the v=0 state
	//(awkwardness is just a small consequence of ISTLisbon xsec database convention)
	for (int ix = 0; ix < N_vib; ++ix) {

		int state = CO_lib.allspecies[0]->n_exc() - N_vib + ix;
		CO_lib.allspecies[0]->exc[state]->scale(vib_BoltzPop.at(0));
	}


	// ------------------

	CO_lib.assign_fracs_by_names(CO_name, 1.0);

	


	// Set up dipole rotations either analytically or based on the database
	if (USE_ANALYTIC_ROT) {

		for (int J = 0; J < N_rot_dipole; ++J) {

			int Jprime = J + 1;

			// add rotation
			auto X1 = lib::linear_dipole_rotation_DCS(J, Jprime, CO_name, CO_MU, CO_B, true);

			// add reverse superelastic
			auto X2 = lib::linear_dipole_rotation_DCS(Jprime, J, CO_name, CO_MU, CO_B, true);

			switch (rotational_scattering) {
				case ISO: {
					X1.scattering(lib::isotropic_scattering());
					X2.scattering(lib::isotropic_scattering());
					break; }
				case FWD: {
					X1.scattering(lib::forward_scattering());
					X2.scattering(lib::forward_scattering());
					break; }
				case DB: { // taken care of in initial
					break; }
			}


			CO_lib.add_Xsec(X1);
			CO_lib.add_Xsec(X2);
		}
		
		// Carve out rotation species, assign presence
		for (int J = 0; J < N_rot_dipole; ++J) {

			std::string spec = CO_lib.allspecies[N_non_rot_species + J]->name();

			if (T_g == 0 && J == 0) {
				bodies_to_keep.push_back(spec);
			}
			else if (T_g > 0) {
				bodies_to_keep.push_back(spec);
			}

			// with fraction-preservation disabled, weight each J-species as per BoltzPop
			CO_lib.assign_fracs_by_names(spec, rot_BoltzPop(J)); // boltz-pop at J
		}

	}
	else {

		if (USE_LISBON) {
			CO_lib.add_xsecs_from_LXCat_files("../../applications/CO_rot/CO_rot_cross-sections/ISTLisbon/ISTLisbon_CO-rot.txt");
		}
		else{
			CO_lib.add_xsecs_from_LXCat_files("../../applications/CO_rot/CO_rot_cross-sections/MAGBOLTZ_v11-10/CO_dipint-rot.txt");
		}


		//lib::Scattering I_DB = lib::dipole_BornApprox_scattering();

		// Carve out rotation species, assign presence
		// assign presence by scale to rotations belonging to the target over-populated by the tabulation
		CO_lib.assign_frac_by_index(N_non_rot_species, 1.0);
		for (int J = 0; J < N_rot_dipole; ++J) {
			// weight each J-collision as per BoltzPop
			CO_lib.allspecies[N_non_rot_species]->allcollisions[J]->scale(rot_BoltzPop(J)); // boltz-pop at J


			switch (rotational_scattering) {
			case ISO: { // taken care of in initial
				break; }
			case FWD: {
				CO_lib.allspecies[N_non_rot_species]->allcollisions[J]->scattering(lib::forward_scattering());
				break; }
			case DB: { 
				int Jprime = J + 1;
				double EV_THRESH = 2.0 * CO_B * Jprime;

				arma::colvec test_eV = arma::logspace(-3, 3, 20);
				arma::colvec test_eVprime = test_eV - EV_THRESH;

				CO_lib.allspecies[N_non_rot_species]->allcollisions[J]->scattering(lib::dipole_BornApprox_scattering());
				CO_lib.allspecies[N_non_rot_species]->allcollisions[J]->scattering()->find_better_grid_chi(test_eV, test_eVprime);
				break; }
			}

			//if (USE_ANISOTROPIC_SCATTERING_ROT) {
			//	int Jprime = J + 1;
			//	double EV_THRESH = 2.0 * CO_B * Jprime;
			//
			//	arma::colvec test_eV = arma::logspace(-3, 3, 20);
			//	arma::colvec test_eVprime = test_eV - EV_THRESH;
			//
			//	CO_lib.allspecies[N_non_rot_species]->allcollisions[J]->scattering(I_DB);
			//	CO_lib.allspecies[N_non_rot_species]->allcollisions[J]->scattering()->find_better_grid_chi(test_eV, test_eVprime);
			//}

		}

		// weight the superelastics
		for (int J = 1; J < N_rot_dipole; ++J) {
			// weight each J-collision as per BoltzPop
			CO_lib.assign_frac_by_index(N_non_rot_species+J, rot_BoltzPop(J)); // boltz-pop at J


			switch (rotational_scattering) {
			case ISO: { // taken care of in initial
				break; }
			case FWD: {
				CO_lib.allspecies[N_non_rot_species + J]->sup[0]->scattering(lib::forward_scattering());
				break; }
			case DB: {
				double EV_THRESH = 2.0 * CO_B * J;
				arma::colvec test_eV = arma::logspace(-3, 3, 20);
				arma::colvec test_eVprime = test_eV + EV_THRESH;
				CO_lib.allspecies[N_non_rot_species + J]->sup[0]->scattering(lib::dipole_BornApprox_scattering());
				CO_lib.allspecies[N_non_rot_species + J]->sup[0]->scattering()->find_better_grid_chi(test_eVprime, test_eV);
				break; }
			}
			//if (USE_ANISOTROPIC_SCATTERING_ROT) {
			//	double EV_THRESH = 2.0 * CO_B * J;
			//	arma::colvec test_eV = arma::logspace(-3, 3, 20);
			//	arma::colvec test_eVprime = test_eV + EV_THRESH;
			//	CO_lib.allspecies[N_non_rot_species + J]->sup[0]->scattering(I_DB);
			//	CO_lib.allspecies[N_non_rot_species + J]->sup[0]->scattering()->find_better_grid_chi(test_eVprime, test_eV);
			//}
		}

	}

	//// for including quadrupole
	//if (INCLUDE_QUADRUPOLE) {
	//	for (int J = 0; J < N_rot_quadrupole; ++J) {
	//		
	//		int Jprime = J + 2;
	//
	//		// add rotation
	//		auto X = lib::linear_quadrupole_rotation_DCS(J, Jprime, CO_name, CO_Q, CO_B);
	//		CO_lib.add_Xsec(X);
	//
	//		// add reverse superelastic
	//		X = lib::linear_quadrupole_rotation_DCS(Jprime, J, CO_name, CO_Q, CO_B);
	//		CO_lib.add_Xsec(X);
	//		
	//	}
	//}

	// throw away extra species
	//CO_lib.keep_only(bodies_to_keep);
	CO_lib.remove_all_zero_frac();

	std::cout << "Using following library of CO collisions: " << std::endl;
	CO_lib.print();


	// --- Handle calculations and export of data -------------------------------


	std::string export_location = "../../applications/CO_rot/export";
	std::string export_name = mb::UNALLOCATED_STRING;
	mb::sweep_option sweep_option = mb::sweep_option::EN_Td;
	bool export_xsecs = true;
	bool LIMIT_EXPORT = false;

	// Initialize exporter
	
	// Now that all outputs have been collected,
	// Write them in a loop

	auto out_vec = std::vector<mb::BoltzmannOutput>(sweep.size());
	auto p_vec = std::vector<mb::BoltzmannParameters>(sweep.size());
	

	


	#pragma omp parallel
	#pragma omp for
	for (int i = 0; i < sweep.size(); i++){

		// Perform calculation //

		lib::Library this_lib = CO_lib;
		mb::BoltzmannParameters this_p = p;

		this_p.EN_Td = sweep.at(i);

		//this_p.initial_eV_max = vec_eVmax.at(i);

		mb::BoltzmannSolver solver = mb::BoltzmannSolver(this_p, this_lib);	// perform the actual calculation

		out_vec.at(i) = solver.get_output();
		p_vec.at(i) = this_p;
	}

	mb::Exporter exp = mb::Exporter();
	
	std::cout << std::endl << "RESULTS" << std::endl;
	std::cout << "Temperature: " << T_g  << " K" << std::endl;

	if (USE_LISBON) {
		std::cout << "Using the IST-Lisbon set of CO cross sections." << std::endl;
	}
	else {
		std::cout << "Using the Biagi (MAGBOLTZ v11.10) set of CO cross sections." << std::endl;
	}

	//if (INCLUDE_QUADRUPOLE) {
	//	std::cout << "Quadrupole rotatons are included" << std::endl;
	//}
	//else {
	//	std::cout << "Quadrupole rotations are excluded. " << std::endl;
	//}

	if (!(rotational_scattering == ISO)) {
		std::cout << "Dipole rotations are using anisotropic scattering (all others are isotropic)." << std::endl;
	}
	else {
		std::cout << "All collisions are using isotropic scattering." << std::endl;
	}
	
	exp = mb::Exporter(CO_lib, p, out_vec.at(0),
		export_location, export_name, sweep_option, export_xsecs, LIMIT_EXPORT);
	std::cout << std::endl;

	std::cout << "E/N\tmuN_FLUX" << std::endl;
	for (int i = 0; i < out_vec.size(); ++i) {
	
		exp.write_this_run(p_vec.at(i), out_vec.at(i));
	
		printf("%1.4e\t%1.4e\n", sweep.at(i), out_vec.at(i).muN_FLUX);
	}

	return 0;
}


